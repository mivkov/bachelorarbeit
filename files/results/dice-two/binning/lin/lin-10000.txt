 _/_/_/       _/_/     _/    _/    _/_/_/   _/_/_/_/    _/_/_/  
 _/    _/    _/  _/    _/_/_/_/   _/    _/  _/         _/    _/ 
 _/    _/   _/    _/   _/ _/ _/   _/        _/         _/       
 _/_/_/     _/_/_/_/   _/    _/     _/_/    _/_/_/       _/_/   
 _/    _/   _/    _/   _/    _/         _/  _/               _/ 
 _/    _/   _/    _/   _/    _/   _/    _/  _/         _/    _/ 
 _/    _/   _/    _/   _/    _/    _/_/_/   _/_/_/_/    _/_/_/  
                         Version 3.0                            
        written by Romain Teyssier (University of Zurich)       
                (c) CEA 1999-2007, UZH 2008-2014                
  
 Working with nproc =    4 for ndim = 3
 Using solver = hydro with nvar =  5
  
 compile date = 11/22/16-14:47:55
 patch dir    = ../patch/unbinding:../patch/init/dice
 remote repo  = https://mivkov@bitbucket.org/mivkov/bachelorarbeit.git
 local branch = master
 last commit  = 1c3eb211b8ccb5a0e1c9b81f50f7e39605f902fd
  
 Hydro var indices:
 Building initial AMR grid
 Opening -> ../../files/ic_files/dice/final//two.g2                                         
__________________________________________________
 -> Found HEAD block
 -> Found POS  block
 -> Found VEL  block
 -> Found ID   block
 -> Found MASS block
 -> Found Z    block
__________________________________________________
 Found       240000  particles
 ---->            0  type 0 particles
 ---->       240000  type 1 particles
 ---->            0  type 2 particles
 ---->            0  type 3 particles
 ---->            0  type 4 particles
 ---->            0  type 5 particles
__________________________________________________
 Gadget2 file successfully loaded
__________________________________________________
 Gas mass in AMR grid ->  0.000E+00 unit_m
__________________________________________________
  npart_tot ->       240000
__________________________________________________
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
 Load balancing AMR grid...
__________________________________________________
 Remaining particles ->         240000
__________________________________________________
 Time elapsed since startup:   23.116352081298828     
 Initial mesh structure
 Level  1 has          1 grids (       0,       1,       0,)
 Level  2 has          8 grids (       2,       2,       2,)
 Level  3 has         64 grids (      14,      18,      16,)
 Level  4 has        512 grids (     114,     142,     128,)
 Level  5 has       3751 grids (     837,    1038,     937,)
 Level  6 has       9161 grids (    1992,    2559,    2290,)
 Level  7 has       9228 grids (    2060,    2578,    2307,)
 Level  8 has       5550 grids (    1292,    1484,    1387,)
 Level  9 has       1958 grids (     390,     600,     489,)
 Level 10 has        328 grids (      54,     107,      82,)
 Starting time integration
 Load balancing AMR grid...
 Total number of cells above threshold=      193556
 Total number of density peaks found=      5598
 Finding peak patches
 istep=           1 nmove=      177930
 istep=           2 nmove=        8397
 istep=           3 nmove=        1256
 istep=           4 nmove=         280
 istep=           5 nmove=          92
 istep=           6 nmove=           3
 istep=           7 nmove=           0
 Now merging irrelevant peaks.
 niter=           1 nmove=        4842
 niter=           2 nmove=        1237
 niter=           3 nmove=         444
 niter=           4 nmove=         149
 niter=           5 nmove=          46
 niter=           6 nmove=          25
 niter=           7 nmove=           0
 level=           0 nmove=        4842 survived=         756
 niter=           1 nmove=         667
 niter=           2 nmove=         229
 niter=           3 nmove=          83
 niter=           4 nmove=          15
 niter=           5 nmove=           0
 level=           1 nmove=         667 survived=          89
 niter=           1 nmove=          82
 niter=           2 nmove=           7
 niter=           3 nmove=           0
 level=           2 nmove=          82 survived=           7
 niter=           1 nmove=           5
 niter=           2 nmove=           0
 level=           3 nmove=           5 survived=           2
 niter=           1 nmove=           0
 level=           4 nmove=           0 survived=           2
 Found           2  relevant peaks
 Computing relevant clump properties.
 Now merging peaks into halos.
 niter=           1 nmove=           1
 niter=           2 nmove=           0
 level=           0 nmove=           1 survived=           1
 niter=           1 nmove=           0
 level=           1 nmove=           0 survived=           1
 Found           1  halos
   entered unbinding for poisson potential and linbins with  10000  bins
   entered unbinding for poisson potential and linbins with  10000  bins
   entered unbinding for poisson potential and linbins with  10000  bins
   entered unbinding for poisson potential and linbins with  10000  bins
 Started unbinding.
 ---Entered particleoutput
 Found     45911     unbound particles out of      52394  candidates
 ---Entered particleoutput
 Output status of peak memory.
 peaks per cpu
    1450     1362     1407     1379
 ghost peaks per cpu
     643      879      775      639
 hash table collisions
       0        0        0        0
 sparse matrix used
   48140    49589    48450    45308
 Outputing clump properties to disc.
 Total mass above threshold = 1.18902E+03
 Total mass in         2 listed clumps = 1.18902E+03
   ==> Level=    3 Step=    4 Error= 2.411E-05
   ==> Level=    4 Step=    4 Error= 4.680E-05
   ==> Level=    5 Step=    8 Error= 7.602E-05
   ==> Level=    6 Step=    4 Error= 5.198E-05
   ==> Level=    7 Step=    4 Error= 5.536E-05
   ==> Level=    8 Step=    4 Error= 6.047E-05
   ==> Level=    9 Step=    4 Error= 6.485E-05
   ==> Level=   10 Step=    4 Error= 6.561E-05
 Fine step=      0 t= 0.00000E+00 dt= 3.587E-02 a= 1.000E+00 mem= 2.4%  6.0%
   ==> Level=   10 Step=    4 Error= 6.339E-05
 Fine step=      1 t= 3.58722E-02 dt= 3.587E-02 a= 1.000E+00 mem= 2.4%  6.0%
   ==> Level=    9 Step=    4 Error= 6.394E-05
   ==> Level=   10 Step=    4 Error= 6.477E-05
 Fine step=      2 t= 7.17445E-02 dt= 3.596E-02 a= 1.000E+00 mem= 2.4%  6.0%
   ==> Level=   10 Step=    4 Error= 6.492E-05
 Fine step=      3 t= 1.07705E-01 dt= 3.596E-02 a= 1.000E+00 mem= 2.4%  6.0%
   ==> Level=    8 Step=    4 Error= 6.059E-05
   ==> Level=    9 Step=    4 Error= 6.456E-05
   ==> Level=   10 Step=    4 Error= 5.779E-05
 Fine step=      4 t= 1.43666E-01 dt= 3.605E-02 a= 1.000E+00 mem= 2.4%  6.0%
   ==> Level=   10 Step=    4 Error= 5.758E-05
 Fine step=      5 t= 1.79720E-01 dt= 3.605E-02 a= 1.000E+00 mem= 2.4%  6.0%
   ==> Level=    9 Step=    4 Error= 6.416E-05
   ==> Level=   10 Step=    4 Error= 6.320E-05
 Fine step=      6 t= 2.15773E-01 dt= 3.614E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=   10 Step=    4 Error= 6.307E-05
 Fine step=      7 t= 2.51908E-01 dt= 3.614E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=    7 Step=    4 Error= 5.558E-05
   ==> Level=    8 Step=    4 Error= 6.054E-05
   ==> Level=    9 Step=    4 Error= 6.305E-05
   ==> Level=   10 Step=    4 Error= 6.680E-05
 Fine step=      8 t= 2.88044E-01 dt= 3.610E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=   10 Step=    4 Error= 6.605E-05
 Fine step=      9 t= 3.24142E-01 dt= 3.610E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=    9 Step=    4 Error= 6.279E-05
   ==> Level=   10 Step=    4 Error= 6.357E-05
 Fine step=     10 t= 3.60240E-01 dt= 3.610E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=   10 Step=    4 Error= 6.314E-05
 Fine step=     11 t= 3.96339E-01 dt= 3.610E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=    8 Step=    4 Error= 6.052E-05
   ==> Level=    9 Step=    4 Error= 6.355E-05
   ==> Level=   10 Step=    4 Error= 6.704E-05
 Fine step=     12 t= 4.32437E-01 dt= 3.607E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=   10 Step=    4 Error= 6.649E-05
 Fine step=     13 t= 4.68510E-01 dt= 3.607E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=    9 Step=    4 Error= 6.331E-05
   ==> Level=   10 Step=    4 Error= 7.295E-05
 Fine step=     14 t= 5.04583E-01 dt= 3.595E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=   10 Step=    4 Error= 7.176E-05
 Fine step=     15 t= 5.40530E-01 dt= 3.595E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=    6 Step=    4 Error= 5.218E-05
   ==> Level=    7 Step=    4 Error= 5.538E-05
   ==> Level=    8 Step=    4 Error= 6.066E-05
   ==> Level=    9 Step=    4 Error= 6.291E-05
   ==> Level=   10 Step=    4 Error= 7.593E-05
 Fine step=     16 t= 5.76476E-01 dt= 3.582E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=   10 Step=    4 Error= 7.493E-05
 Fine step=     17 t= 6.12293E-01 dt= 3.582E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=    9 Step=    4 Error= 6.279E-05
   ==> Level=   10 Step=    4 Error= 6.657E-05
 Fine step=     18 t= 6.48110E-01 dt= 3.568E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=   10 Step=    4 Error= 6.693E-05
 Fine step=     19 t= 6.83794E-01 dt= 3.568E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=    8 Step=    4 Error= 6.089E-05
   ==> Level=    9 Step=    4 Error= 6.391E-05
   ==> Level=   10 Step=    4 Error= 6.430E-05
 Fine step=     20 t= 7.19479E-01 dt= 3.555E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=   10 Step=    4 Error= 6.413E-05
 Fine step=     21 t= 7.55030E-01 dt= 3.555E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=    9 Step=    4 Error= 6.370E-05
   ==> Level=   10 Step=    4 Error= 6.071E-05
 Fine step=     22 t= 7.90580E-01 dt= 3.542E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=   10 Step=    4 Error= 5.935E-05
 Fine step=     23 t= 8.25997E-01 dt= 3.542E-02 a= 1.000E+00 mem= 2.4%  6.1%
   ==> Level=    7 Step=    4 Error= 5.549E-05
   ==> Level=    8 Step=    4 Error= 6.097E-05
   ==> Level=    9 Step=    4 Error= 6.359E-05
   ==> Level=   10 Step=    4 Error= 5.935E-05
 Fine step=     24 t= 8.61414E-01 dt= 3.528E-02 a= 1.000E+00 mem= 2.4%  6.2%
   ==> Level=   10 Step=    4 Error= 5.798E-05
 Fine step=     25 t= 8.96698E-01 dt= 3.528E-02 a= 1.000E+00 mem= 2.4%  6.2%
   ==> Level=    9 Step=    4 Error= 6.369E-05
   ==> Level=   10 Step=    4 Error= 5.522E-05
 Fine step=     26 t= 9.31983E-01 dt= 3.515E-02 a= 1.000E+00 mem= 2.4%  6.2%
   ==> Level=   10 Step=    4 Error= 5.465E-05
 Fine step=     27 t= 9.67136E-01 dt= 3.515E-02 a= 1.000E+00 mem= 2.4%  6.2%
   ==> Level=    8 Step=    4 Error= 6.096E-05
   ==> Level=    9 Step=    4 Error= 6.328E-05
   ==> Level=   10 Step=    4 Error= 5.735E-05
 Fine step=     28 t= 1.00229E+00 dt= 3.502E-02 a= 1.000E+00 mem= 2.4%  6.2%
   ==> Level=   10 Step=    4 Error= 5.734E-05
 Fine step=     29 t= 1.03731E+00 dt= 3.502E-02 a= 1.000E+00 mem= 2.4%  6.2%
   ==> Level=    9 Step=    4 Error= 6.342E-05
   ==> Level=   10 Step=    4 Error= 6.260E-05
 Fine step=     30 t= 1.07234E+00 dt= 3.489E-02 a= 1.000E+00 mem= 2.4%  6.2%
   ==> Level=   10 Step=    4 Error= 6.278E-05
 Fine step=     31 t= 1.10723E+00 dt= 3.489E-02 a= 1.000E+00 mem= 2.4%  6.2%
 Time elapsed since last coarse step:   19.12 s       74.40 mus/pt       74.40 mus/pt (av)
 Used memory: 409.9 Mb
 Total number of cells above threshold=      194145
 Total number of density peaks found=      5660
 Finding peak patches
 istep=           1 nmove=      178052
 istep=           2 nmove=        8651
 istep=           3 nmove=        1389
 istep=           4 nmove=         308
 istep=           5 nmove=          78
 istep=           6 nmove=           7
 istep=           7 nmove=           0
 Now merging irrelevant peaks.
 niter=           1 nmove=        4852
 niter=           2 nmove=        1094
 niter=           3 nmove=         359
 niter=           4 nmove=         121
 niter=           5 nmove=          55
 niter=           6 nmove=          23
 niter=           7 nmove=           1
 niter=           8 nmove=           0
 level=           0 nmove=        4852 survived=         808
 niter=           1 nmove=         702
 niter=           2 nmove=         307
 niter=           3 nmove=         110
 niter=           4 nmove=          28
 niter=           5 nmove=           2
 niter=           6 nmove=           0
 level=           1 nmove=         702 survived=         106
 niter=           1 nmove=          95
 niter=           2 nmove=           7
 niter=           3 nmove=           0
 level=           2 nmove=          95 survived=          11
 niter=           1 nmove=           9
 niter=           2 nmove=           0
 level=           3 nmove=           9 survived=           2
 niter=           1 nmove=           0
 level=           4 nmove=           0 survived=           2
 Found           2  relevant peaks
 Computing relevant clump properties.
 Now merging peaks into halos.
 niter=           1 nmove=           1
 niter=           2 nmove=           0
 level=           0 nmove=           1 survived=           1
 niter=           1 nmove=           0
 level=           1 nmove=           0 survived=           1
 Found           1  halos
   entered unbinding for poisson potential and linbins with  10000  bins
   entered unbinding for poisson potential and linbins with  10000  bins
   entered unbinding for poisson potential and linbins with  10000  bins
   entered unbinding for poisson potential and linbins with  10000  bins
 Started unbinding.
 ---Entered particleoutput
 Found     44875     unbound particles out of      50460  candidates
 ---Entered particleoutput
 Output status of peak memory.
 peaks per cpu
    1440     1391     1434     1395
 ghost peaks per cpu
     672      799      762      670
 hash table collisions
       0        0        0        0
 sparse matrix used
   48470    48308    47921    47166
 Outputing clump properties to disc.
 Total mass above threshold = 1.18909E+03
 Total mass in         2 listed clumps = 1.18909E+03
   ==> Level=    3 Step=    4 Error= 2.411E-05
   ==> Level=    4 Step=    4 Error= 4.681E-05
   ==> Level=    5 Step=    4 Error= 7.654E-05
   ==> Level=    6 Step=    4 Error= 5.223E-05
   ==> Level=    7 Step=    4 Error= 5.587E-05
   ==> Level=    8 Step=    4 Error= 6.126E-05
   ==> Level=    9 Step=    4 Error= 6.568E-05
   ==> Level=   10 Step=    4 Error= 7.240E-05
 Mesh structure
 Level  1 has          1 grids (       0,       1,       0,)
 Level  2 has          8 grids (       2,       2,       2,)
 Level  3 has         64 grids (      14,      18,      16,)
 Level  4 has        512 grids (     114,     142,     128,)
 Level  5 has       3751 grids (     837,    1038,     937,)
 Level  6 has       9161 grids (    1992,    2559,    2290,)
 Level  7 has       9293 grids (    2133,    2607,    2323,)
 Level  8 has       5617 grids (    1364,    1448,    1404,)
 Level  9 has       1942 grids (     375,     547,     485,)
 Level 10 has        301 grids (      48,     113,      75,)
 Main step=      1 mcons= 0.00E+00 econs= 0.00E+00 epot=-4.06E+03 ekin= 4.60E+03
 Fine step=     32 t= 1.14212E+00 dt= 3.477E-02 a= 1.000E+00 mem= 2.4%  6.2%
 Run completed
 Total elapsed time:   18.654561996459961     
 --------------------------------------------------------------------

     minimum       average       maximum  standard dev        std/av       %   rmn   rmx  TIMER
       0.428         0.428         0.429         0.000         0.001     1.5     2   1    refine                  
       0.278         0.278         0.278         0.000         0.000     1.0     1   2    load balance            
       1.555         1.633         1.686         0.049         0.030     5.7     1   4    particles               
      17.406        17.407        17.409         0.001         0.000    60.5     3   4    io                      
       7.135         7.135         7.135         0.000         0.000    24.8     3   2    poisson                 
       1.200         1.218         1.237         0.013         0.011     4.2     4   1    rho                     
       0.119         0.142         0.175         0.020         0.143     0.5     4   1    courant                 
       0.524         0.544         0.569         0.018         0.033     1.9     2   1    flag                    
      28.788     100.0    TOTAL
