! This file contains:
! subroutine mladen_printposition                                     line 21
! subroutine mladen_getclumpparticlenumber()                          line 80
! subroutine mladen_getclumpparticles_withoutcellindex()              line 236
! subroutine mladen_writetoonefile()                                  line 339
! subroutine mladen_gettotalpeaks()                                   line 386
! subroutine mladen_getclumpparticles_withoutlinkedlist()             line 426
! subroutine mladen_write_clump_map(relevant_output)                  line 553
! subroutine mladen_getmasscomparison_old()                           line 690
! subroutine getparticleID_loopoverparticles()                        line 850 
! subroutine mladen_particlecheck()                                   line 944
! subroutine mladen_getmasscomparison2()                              line 1052
! subroutine getparticleID_loopoverparticles_use_nvector              line 1173
! real(dp) function enclosed_mass(ipeak, distance)                    line 1287 
! logical function unbound(ipeak, part_ind)                           line 1337
! real(dp) function potential(ipeak, distance)                        line 1373
! real(dp) function a_lin(ipeak,ibin)                                 line 1442
! subroutine get_periodicity_correction()                             line 1521
! subroutine mladen_particlecount()                                   line 1553
!
!######################################################################
subroutine mladen_printposition(global_peak_id, cpuid)
    use amr_commons
    use clfind_commons      
    use pm_commons !using mp
    use amr_parameters
    use hydro_commons !using mass_sph
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif

    integer, intent(in) :: global_peak_id, cpuid
    ! find and print the x, y and z positions for all particles in peak
    ! global_peak_id to file in order to compare it with clump_output
    ! To avoid MPI complications, you must give the myid where the
    ! clump in the output is located.

    integer :: local_peak_id, ind, grid, ipart, counter, info, numbpar, ip, this_global_peak_id
    character(len=5) :: nchar, ncharid
    character(len=30) :: filename

    counter = 0
    if (myid==cpuid) then 
        call get_local_peak_id(global_peak_id, local_peak_id)
        call title(global_peak_id, ncharid)
        call title(ifout-1, nchar)
        filename = TRIM('output_'//nchar//'/'//ncharid)
        open(unit=666, file=filename, form='formatted')
        write(666, '(3A18)') "x", "y", "z"

        do counter=1, ntest !loop over all test particles
            this_global_peak_id=flag2(icellp(counter))

            if (global_peak_id == this_global_peak_id) then
                ind=(icellp(counter)-ncoarse-1)/ngridmax+1 ! get cell position
                grid=icellp(counter)-ncoarse-(ind-1)*ngridmax ! get grid index
            
                !get number of particles in grid
                numbpar = numbp(grid)
                if(numbpar > 0) then
                    do ip=1, numbpar
                        ipart=headp(grid)
                        write(666, '(3E18.9E2)') xp(ipart, 1), xp(ipart, 2), xp(ipart, 3)
                        ipart=nextp(ipart) 
                    end do
                end if
            end if
        end do
        
        close(666)
    end if !myid == ncpu



end subroutine mladen_printposition

!#######################################################################

subroutine mladen_getclumpparticlenumber()
! gets the number of particles of all clumps and writes it to
! output_XXXXX/mladen_clumpparticlenumber.txt

    use amr_commons
    use clfind_commons      
    use pm_commons !using mp
    use amr_parameters
    use hydro_commons !using mass_sph
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif
    integer :: counter, global_npeak_sum

    !dummy arguments
    !integer, dimension(1:npeaks_max) , intent(in):: ind_sort
    !real(dp) :: particle_mass

    ! for looping over test particles and getting peak ids
    integer :: ipart, global_peak_id, local_peak_id 
    
    ! getting particles per peak my way
    integer :: info, ind, grid
    integer, dimension(1:ncpu):: npeak_all, npeak_tot

    !getting particle mass
    real(dp) :: particle_mass, particle_mass_tot

    integer, dimension(:), allocatable :: particles_per_peak, particles_per_peak_tot

    !filename
    character(len=80) :: fileloc
    character(len=5) :: nchar, nchar2
    !#############################



    !#############################
    !!generate filename
    !!not really needed...
    !call title(ifout-1, nchar)
    !call title(myid, nchar2)
    !fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_clumpparticlenumber.txt'//nchar2)
    !#############################



    !#############################
    !get particle mass (copied from subroutine write_clump_properties)
    if(ivar_clump==0)then
        particle_mass=MINVAL(mp, MASK=(mp.GT.0.))
#ifndef WITHOUTMPI  
        call MPI_ALLREDUCE(particle_mass,particle_mass_tot,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,info)
        particle_mass=particle_mass_tot  
#endif
    else
        if(hydro)then
            particle_mass=mass_sph
        endif
    endif
    !#############################




    !#############################
    ! PEAK - STUFF
    ! how many peaks per cpu
    npeak_all=0
    npeak_all(myid) = npeaks
    call MPI_ALLREDUCE(npeak_all,npeak_tot,ncpu,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
    
    !total amount of peaks
    global_npeak_sum = sum(npeak_tot)





    ! how many particles per peak
    allocate(particles_per_peak(1:global_npeak_sum))
    allocate(particles_per_peak_tot(1:global_npeak_sum))
    particles_per_peak = 0
    particles_per_peak_tot = 0

    !#############################




    !#############################
    ! MAIN LOOP
    do ipart=1, ntest !loop over all test cells
        global_peak_id=flag2(icellp(ipart))

        if (global_peak_id /= 0) then

            ! get local peak id
            call get_local_peak_id(global_peak_id, local_peak_id)

            ! if peak relevant:
            if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
            
                ind=(icellp(ipart)-ncoarse-1)/ngridmax+1 ! get cell position
                grid=icellp(ipart)-ncoarse-(ind-1)*ngridmax ! get grid index
            
                !get number of particles in grid
                particles_per_peak(global_peak_id) =  particles_per_peak(global_peak_id) + numbp(grid) 
            end if
        end if

    end do

    ! add the global particles per peak list
    call MPI_ALLREDUCE(particles_per_peak, particles_per_peak_tot, global_npeak_sum, MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)




    !if (myid==1) then
    !    write(6, *)   
    !    write(6, *) '#################################'
    !    write(6, *) '#################################'
    !    write(6, *) ' FROM getclumpparticles'
    !    write(6, *) 'npeak_all', npeak_all
    !    write(6, *) 'npeak_tot', npeak_tot
    !    write(6, *) 'sum(npeak_tot)', global_npeak_sum
    !    write(6, *) 'size(particles_per_peak_tot)', size(particles_per_peak_tot)
    !    write(6, *) 
    !    write(6, *) '#################################'
    !    write(6, *) '#################################'
    !    write(6, *)
    !end if
    
    
    if(myid==1) then
        open(unit=777, file=TRIM('output_'//nchar//'/mladen_clumpparticlenumber.txt'), form='formatted')
        
        write(777, '(2A18)') "global peak id", "nr of prtcls"
        do ind=1, sum(npeak_tot)
            if(particles_per_peak_tot(ind) /= 0) then
                write(777, '(2I18)') ind, particles_per_peak_tot(ind)
            end if
        end do

        close(777)
    end if
  

end subroutine mladen_getclumpparticlenumber


!#######################################################################

subroutine mladen_getclumpparticles_withoutcellindex()

    ! Writes position, velocity and mass of all particles in clumps
    ! to file mladen_clumpparticles.txt00***

    use amr_commons
    use clfind_commons      
    use pm_commons !using mp
    use amr_parameters
    use hydro_commons !using mass_sph
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif
    !integer :: counter, global_npeak_sum

    !dummy arguments

    ! for looping over test cells and getting particle list
    integer :: itestpart, ipart,this_part, global_peak_id, local_peak_id, part_in_grid 
    
    ! getting particles per peak
    integer :: info, ind, grid
    integer, dimension(1:ncpu):: npeak_all, npeak_tot

    !getting particle mass
    real(dp) :: particle_mass, particle_mass_tot

    !integer, dimension(:), allocatable :: particles_per_peak, particles_per_peak_tot

    !filename
    character(len=80) :: fileloc
    character(len=5) :: nchar, nchar2
    
    !#############################


    !#############################
    !get particle mass (copied from subroutine write_clump_properties)
    if(ivar_clump==0)then
        particle_mass=MINVAL(mp, MASK=(mp.GT.0.))
#ifndef WITHOUTMPI  
        call MPI_ALLREDUCE(particle_mass,particle_mass_tot,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,info)
        particle_mass=particle_mass_tot  
#endif
    else
        if(hydro)then
            particle_mass=mass_sph
        endif
    endif
    !#############################



    !#############################
    !generate filename
    call title(ifout-1, nchar)
    call title(myid, nchar2)
    fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_clumpparticles.txt'//nchar2)
    !#############################

    !#############################
    ! MAIN LOOP

    open(unit=666, file=fileloc, form='formatted')
    write(666, '(8A18)') "peak index", "x coordinate", "y coordinate", "z coordinate", "vx", "vy", "vz", "particle mass"

    do itestpart=1, ntest !loop over all test cells
        global_peak_id=flag2(icellp(itestpart))

        if (global_peak_id /= 0) then

            ! get local peak id
            call get_local_peak_id(global_peak_id, local_peak_id)

            ! if peak relevant:
            if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
            
                ind=(icellp(itestpart)-ncoarse-1)/ngridmax+1 ! get cell position
                grid=icellp(itestpart)-ncoarse-(ind-1)*ngridmax ! get grid index
            
                !get number of particles in grid
                part_in_grid = numbp(grid)
                do ipart = 1, part_in_grid
                    this_part=headp(grid)
                    write(666, '(I18,7E18.9E2)') global_peak_id, xp(this_part, 1), xp(this_part, 2), xp(this_part, 3),vp(this_part, 1), vp(this_part, 2), vp(this_part, 3), mp(this_part)
                    this_part = nextp(this_part)
                end do
            end if
        end if
    end do

    close(666)

end subroutine mladen_getclumpparticles_withoutcellindex


!########################################
!########################################
!########################################


subroutine mladen_writetoonefile(somemessage)
    use amr_commons
    use clfind_commons

    implicit none
    include 'mpif.h'
    integer, parameter :: tag_mladen = 666
    integer :: dummy_io, info2
    character(len=*), intent(in) :: somemessage

    ! local vars
    integer :: i
    character (len=80) :: filename
    character (len=5) :: nchar
    character (len=10) :: mladen_string, str

    call title(ifout-1, nchar)
    filename=TRIM('output_'//TRIM(nchar)//'/mladen_output.txt')

    dummy_io = myid

    if (myid == 1) then
        call mladen_message("Myid=1 found")
        write(mladen_string, '(I2)') ncpu
        call mladen_message("ncpu = "//mladen_string)

        open(unit=666, file=filename, form='formatted')
        write(666, *) 'SOME MESSAGE        MY ID      ACTUAL ID'
        write(666, *) somemessage, dummy_io, 0
        do i = 2, ncpu
            call mpi_recv(dummy_io, 1, mpi_integer, i-1, tag_mladen, mpi_comm_world, MPI_STATUS_IGNORE, info2)
            write(666, *) somemessage, dummy_io, i-1
        end do
        close(666)
    else
        write(mladen_string, '(I2)') myid
        call mladen_message("Myid ="//mladen_string)
        call mpi_send(dummy_io, 1, mpi_integer, 0, tag_mladen, mpi_comm_world, MPI_STATUS_IGNORE, info2)      
    end if

end subroutine mladen_writetoonefile


!########################################
!########################################
!########################################

subroutine mladen_gettotalpeaks(mladen_globalpeaktot)
! gets the number of particles of all clumps and writes it to
! output_XXXXX/mladen_clumpparticlenumber.txt
! used in subroutine mladen_getmasscomparison()

    use amr_commons
    use clfind_commons ! npeaks     
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif

    integer, intent(out) :: mladen_globalpeaktot

    integer :: info
    integer, dimension(1:ncpu):: npeak_all, npeak_tot

    !#############################
    ! how many peaks per cpu
    npeak_all=0
    npeak_tot=0
    npeak_all(myid) = npeaks
    call MPI_ALLREDUCE(npeak_all,npeak_tot,ncpu,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
    
    !total amount of peaks
    mladen_globalpeaktot = sum(npeak_tot)
    if (myid==ncpu) then 
        write(*, *) '#########################'
        write(*, *) mladen_globalpeaktot
        write(*, *) ipeak_start(ncpu)+npeaks
        write(*, *) '#########################'
    end if
end subroutine mladen_gettotalpeaks

!###############################################################
!###############################################################
!###############################################################


subroutine mladen_getclumpparticles_withoutlinkedlist()

    ! Writes position, velocity and mass of all particles in clumps
    ! to file mladen_clumpparticles.txt00***

    use amr_commons
    use clfind_commons      
    use pm_commons !using mp
    use amr_parameters
    use hydro_commons !using mass_sph
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif
    !integer :: counter, global_npeak_sum

    !dummy arguments

    ! for looping over test cells and getting particle list
    integer :: itestcell, ipart,this_part, global_peak_id, local_peak_id, prtcls_in_grid 
    
    ! getting particles per peak
    integer :: info, ind, grid

    !getting particle mass
    real(dp) :: particle_mass, particle_mass_tot

    !getting in which cell of a grid a particle is
    integer :: part_cell_ind,i,j,k

    !counting cells which contain no particles
    integer :: emptycells = 0, emptycells_tot

    !filename
    character(len=80) :: fileloc
    character(len=5) :: nchar, nchar2
    
    !#############################
    !#############################

    !get particle mass (copied from subroutine write_clump_properties)
    if(ivar_clump==0)then
        particle_mass=MINVAL(mp, MASK=(mp.GT.0.))
#ifndef WITHOUTMPI  
        call MPI_ALLREDUCE(particle_mass,particle_mass_tot,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,info)
        particle_mass=particle_mass_tot  
#endif
    else
        if(hydro)then
            particle_mass=mass_sph
        endif
    endif



    !generate filename
    call title(ifout-1, nchar)
    call title(myid, nchar2)
    fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_clumpparticles.txt'//nchar2)

    !#############################
    ! MAIN LOOP

    open(unit=666, file=fileloc, form='formatted')
    write(666, '(8A18)') "peak index", "x coordinate", "y coordinate", "z coordinate", "vx", "vy", "vz", "particle mass"

    !fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_gridinfo.txt'//nchar2)
    !open(unit=667, file=fileloc, form='formatted')
    !write(667, '(4A12)') "global id", "grid", "ind", "npart"

    !emptycells=0
    do itestcell=1, ntest !loop over all test cells
        global_peak_id=flag2(icellp(itestcell))

        if (global_peak_id /= 0) then

            ! get local peak id
            call get_local_peak_id(global_peak_id, local_peak_id)

            ! if peak relevant:
            if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
            
                ind=(icellp(itestcell)-ncoarse-1)/ngridmax+1 ! get cell position
                grid=icellp(itestcell)-ncoarse-(ind-1)*ngridmax ! get grid index
                !get number of particles in grid
                prtcls_in_grid = numbp(grid)
                !if (prtcls_in_grid==0) then
                    !write(*,'(2(A6,I6))') "myid", myid, "peak", global_peak_id
                    !emptycells=emptycells+1 
                this_part=headp(grid)
                !end if

                do ipart = 1, prtcls_in_grid
                    i=0
                    j=0
                    k=0
                    if(xg(grid,1)-xp(this_part,1).lt.0) i=1
                    if(xg(grid,2)-xp(this_part,2).lt.0) j=1
                    if(xg(grid,3)-xp(this_part,3).lt.0) k=1 
                    
                    part_cell_ind=i+2*j+4*k

                    if (part_cell_ind==ind) then
                        write(666, '(I18,7E18.9E2)') global_peak_id, xp(this_part, 1), xp(this_part, 2), xp(this_part, 3),vp(this_part, 1), vp(this_part, 2), vp(this_part, 3), mp(this_part)
                        !write(667, '(4I12)') global_peak_id, grid, ind, prtcls_in_grid
                        clidp(this_part)=global_peak_id
                    end if    
                    this_part = nextp(this_part)
                end do
            end if
        end if
    end do

    !write(*, '(2(A12,I8))') "myid", myid, "emptycells", emptycells
    call MPI_REDUCE(emptycells, emptycells_tot, 1, MPI_INTEGER, MPI_SUM, 0, MPI_COMM_WORLD, info)
    if (myid==1) write(*, '(A20, I10)') "Total empty cells:", emptycells_tot

    close(666)
    !close(667)

end subroutine mladen_getclumpparticles_withoutlinkedlist

!#########################################################
!#########################################################
!#########################################################

subroutine mladen_write_clump_map(relevant_output)
  use amr_commons
  use clfind_commons
  use hydro_commons !using mass_sph
  use pm_commons !using mp
  implicit none
  integer, intent(in) :: relevant_output
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !---------------------------------------------------------------------------
  ! This routine writes a csv-file of cell center coordinates and clump number
  ! for each cell which is in a clump. Makes only sense to be called when the 
  ! clump finder is called at output-writing and not for sink-formation.
  ! relevant_output: 0 write all output without relevance conditions
  !           1 write output with relevance condition (same as clump_output)
  !---------------------------------------------------------------------------

  integer::ind,grid,ix,iy,iz,ipart,nx_loc,peak_nr
  real(dp)::scale,dx
  real(dp) :: particle_mass, particle_mass_tot
  real(dp),dimension(1:3)::xcell,skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  character(LEN=5)::myidstring,nchar,ncharcpu
  integer,parameter::tag=1102
  integer::dummy_io,info2, local_peak_id
  integer:: cells_in_clump, cells_in_clump_tot
  
  nx_loc=(icoarse_max-icoarse_min+1)
  scale=boxlen/dble(nx_loc)

  do ind=1,twotondim
     iz=(ind-1)/4
     iy=(ind-1-4*iz)/2
     ix=(ind-1-2*iy-4*iz)
     xc(ind,1)=(dble(ix)-0.5D0)
     xc(ind,2)=(dble(iy)-0.5D0)
     xc(ind,3)=(dble(iz)-0.5D0)
  end do

  !prepare file output for peak map
  ! Wait for the token
#ifndef WITHOUTMPI
     if(IOGROUPSIZE>0) then
        if (mod(myid-1,IOGROUPSIZE)/=0) then
           call MPI_RECV(dummy_io,1,MPI_INTEGER,myid-1-1,tag,&
                & MPI_COMM_WORLD,MPI_STATUS_IGNORE,info2)
        end if
     endif
#endif


  !get particle_mass for relevance condition
  if (relevant_output>0) then
      if(ivar_clump==0)then
          particle_mass=MINVAL(mp, MASK=(mp.GT.0.))
#ifndef WITHOUTMPI  
          call MPI_ALLREDUCE(particle_mass,particle_mass_tot,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,info2)
          particle_mass=particle_mass_tot  
#endif
      else
          if(hydro)then
              particle_mass=mass_sph
          endif
      endif
  end if

  call title(ifout-1,nchar)
  call title(myid,myidstring)
  if(IOGROUPSIZEREP>0)then
     call title(((myid-1)/IOGROUPSIZEREP)+1,ncharcpu)
     open(unit=20,file=TRIM('output_'//TRIM(nchar)//'/group_'//TRIM(ncharcpu)//'/clump_map.csv'//myidstring),form='formatted')
  else
     open(unit=20,file=TRIM('output_'//TRIM(nchar)//'/clump_map.csv'//myidstring),form='formatted')
     write(20, '(A1,4A14,A8)') "#", "x", "y", "z","cell area", "peak id"
  endif
  !loop parts
  cells_in_clump=0
  do ipart=1,ntest     
     peak_nr=flag2(icellp(ipart)) 


    if (relevant_output>0) then
        call get_local_peak_id(peak_nr, local_peak_id)
        if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
            cells_in_clump=cells_in_clump+1 !counter
            ind=(icellp(ipart)-ncoarse-1)/ngridmax+1 ! cell position
            grid=icellp(ipart)-ncoarse-(ind-1)*ngridmax ! grid index
            dx=0.5D0**levp(ipart)
            xcell(1:ndim)=(xg(grid,1:ndim)+xc(ind,1:ndim)*dx-skip_loc(1:ndim))*scale
            !peak_map
            write(20,'(x,F14.8,F14.8,F14.8,F14.8,I8)')xcell(1),xcell(2),xcell(3),dx*dx,peak_nr
        end if
    else
         if (peak_nr /=0 ) then
            ! Cell coordinates
            cells_in_clump=cells_in_clump+1 !counter
            ind=(icellp(ipart)-ncoarse-1)/ngridmax+1 ! cell position
            grid=icellp(ipart)-ncoarse-(ind-1)*ngridmax ! grid index
            dx=0.5D0**levp(ipart)
            xcell(1:ndim)=(xg(grid,1:ndim)+xc(ind,1:ndim)*dx-skip_loc(1:ndim))*scale
            !peak_map
            write(20,'(x,F14.8,F14.8,F14.8,F14.8,I8)')xcell(1),xcell(2),xcell(3),dx*dx,peak_nr
         end if
    end if
  end do
  close(20)

  ! Send the token
#ifndef WITHOUTMPI
     if(IOGROUPSIZE>0) then
        if(mod(myid,IOGROUPSIZE)/=0 .and.(myid.lt.ncpu))then
           dummy_io=1
           call MPI_SEND(dummy_io,1,MPI_INTEGER,myid-1+1,tag, &
                & MPI_COMM_WORLD,info2)
        end if
     endif
#endif

     call MPI_REDUCE(cells_in_clump, cells_in_clump_tot, 1, MPI_INTEGER, MPI_SUM, 0, MPI_COMM_WORLD, info2)
     if (myid==1) write(*, '(A25, I10)')"Total cells in clumps:", cells_in_clump_tot



end subroutine mladen_write_clump_map




!########################################
!########################################
!########################################





subroutine mladen_getmasscomparison_old()

    ! outdated:
    ! get masscomparison by looping over test cells.
    ! now I do it by looping over linked list.

    use amr_commons
    use clfind_commons   !unbinding stuff is all in here   
    use pm_commons !using mp
    use amr_parameters
    use hydro_commons !using mass_sph
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif

    ! for looping over test cells and getting particle list
    integer :: counter, ipart, this_part, global_peak_id, local_peak_id, prtcls_in_grid 
    
    ! getting particles per peak
    integer :: info, ind, grid

    !getting particle mass
    real(dp) :: particle_mass, particle_mass_tot

    ! peak array
    real(dp), dimension(:), allocatable :: particle_sum_mass, particle_sum_mass_tot, clump_mass_array, clump_mass_array_tot

    !getting in which cell of a grid a particle is
    integer :: part_cell_ind, i,j,k

    real(dp) :: substitute

    !filename
    character(len=80) :: fileloc
    character(len=5) :: nchar
    !#############################

    if(myid==1) write(*,*) "---Entered getmasscomparison"

    !#############################
    !get particle mass (copied from subroutine write_clump_properties)
    if(ivar_clump==0)then
        particle_mass=MINVAL(mp, MASK=(mp.GT.0.))
#ifndef WITHOUTMPI  
        call MPI_ALLREDUCE(particle_mass,particle_mass_tot,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,info)
    particle_mass=particle_mass_tot  
#endif
    else
        if(hydro)then
            particle_mass=mass_sph
        endif
    endif
    !#############################


    !allocate peak and clump_mass arrays

   
    allocate(particle_sum_mass(1:npeaks_tot), particle_sum_mass_tot(1:npeaks_tot), clump_mass_array(1:npeaks_tot), clump_mass_array_tot(1:npeaks_tot))

    particle_sum_mass = 0.0d0
    particle_sum_mass_tot = 0.0d0
    clump_mass_array_tot = 0.0d0
    clump_mass_array = 0.0d0


    if(myid==1) write(*,*) "---Entered getmasscomparison1"
    !#############################
    ! particle mass sum
        do counter=1, ntest !loop over all test cells
        global_peak_id=flag2(icellp(counter))

        if (global_peak_id /= 0) then

            ! get local peak id
            call get_local_peak_id(global_peak_id, local_peak_id)

            ! if peak relevant:
            if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
            
                ind=(icellp(counter)-ncoarse-1)/ngridmax+1 ! get cell position
                grid=icellp(counter)-ncoarse-(ind-1)*ngridmax ! get grid index
            
                !get number of particles in grid
                prtcls_in_grid = numbp(grid)

                !loop over particles in grid
                this_part=headp(grid)

                do ipart = 1, prtcls_in_grid
                    i=0
                    j=0
                    k=0      
                    if(xg(grid,1)-xp(this_part,1).le.0) i=1
                    if(xg(grid,2)-xp(this_part,2).le.0) j=1
                    if(xg(grid,3)-xp(this_part,3).le.0) k=1

                    
                    part_cell_ind=i+2*j+4*k+1

                    if (part_cell_ind==ind) then
                        particle_sum_mass(global_peak_id) = particle_sum_mass(global_peak_id) + mp(this_part)
                    end if
                    this_part = nextp(this_part)
                end do
            end if
        end if
    end do

    if(myid==1) write(*,*) "---Entered getmasscomparison2"
    call MPI_ALLREDUCE(particle_sum_mass, particle_sum_mass_tot, npeaks_tot, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, info)
    
    !clump mass array
    do local_peak_id = 1, npeaks
        global_peak_id = local_peak_id + ipeak_start(myid)
        if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
            clump_mass_array(global_peak_id) = clump_mass(local_peak_id)
        end if
    end do

    call MPI_ALLREDUCE(clump_mass_array, clump_mass_array_tot, npeaks_tot, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, info)

    
    
    if(myid==1) write(*,*) "---Entered getmasscomparison3"
    if (myid==1) then 
        !generate filename
        call title(ifout-1, nchar)
        fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_masscomparison_old.txt')

        open(unit=666, file=fileloc, form='formatted')

        write(666, '(4A20)') "peak index", "mass particles sum", "mass clump", "m_psum/mp_cl"
        !global_peak_id = 0
        do global_peak_id = 1, npeaks_tot
            substitute=0
            if (clump_mass_array_tot(global_peak_id)/=0) substitute=particle_sum_mass_tot(global_peak_id)/clump_mass_array_tot(global_peak_id)

            write(666, '(I20, 3E20.9E2)') global_peak_id, &
            particle_sum_mass_tot(global_peak_id), &
            clump_mass_array_tot(global_peak_id),&
            substitute
        end do

        close(666)
    endif
    if(myid==1) write(*,*) "---Entered getmasscomparison4"
end subroutine mladen_getmasscomparison_old



!########################################
!########################################
!########################################




subroutine getparticleID_loopoverparticles()
    ! This subroutine gets the clump ID for every particle by
    ! looping over all particles instead of test cells.
    ! The reason for this subroutine is to check if I really
    ! get all the particles in a cell.

   ! Get mass comparison using the clump linked lists
    use amr_commons
    use clfind_commons   !unbinding stuff is all in here   
    use pm_commons !using mp
    use amr_parameters
    use hydro_commons !using mass_sph
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif

    integer, dimension(1:nvector) ::  cell_index, cell_levl
    integer, dimension(1:npeaks_max) :: particlesum_control
    integer :: global_peak_id, local_peak_id
    real(dp), dimension(1:nvector, 1:3) :: xcoord

    real(dp) :: particle_mass, particle_mass_tot
    integer :: i, ilevel, info

    !filename
    character(len=80) :: fileloc
    character(len=5) :: nchar, nchar2


    particlesum_control=0

    if(myid==1) write(*,*) "---Entered getparticleID_loopoverparticles"

    !get particle mass (copied from subroutine write_clump_properties)
    if(ivar_clump==0)then
        particle_mass=MINVAL(mp, MASK=(mp.GT.0.))
#ifndef WITHOUTMPI  
        call MPI_ALLREDUCE(particle_mass,particle_mass_tot,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,info)
    particle_mass=particle_mass_tot  
#endif
    else
        if(hydro)then
            particle_mass=mass_sph
        endif
    endif




    do i= 1, npartmax
        if (levelp(i)>0) then
        cell_index=0
        cell_levl=0
            xcoord(1,1:3)=xp(i,1:3)
            call get_cell_index(cell_index, cell_levl,xcoord,nlevelmax,1)
                global_peak_id=flag2(cell_index(1))
                if (global_peak_id>0) then
                    call get_local_peak_id(global_peak_id, local_peak_id)
                    if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
                        clumpid_control(i) = global_peak_id
                        particlesum_control(local_peak_id)=particlesum_control(local_peak_id)+1
                    end if
                end if
        end if
    end do


    call title(ifout-1, nchar)
    call title(myid, nchar2)

    fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_getparticleID_loopoverparticles.txt'//TRIM(nchar2))

    open(unit=666, file=fileloc, form='formatted')

    write(666,'(2A12)') "Peak ID", "partcls in clump"

    do i=1,npeaks
        write(666,'(2I12)') i+ipeak_start(myid), particlesum_control(i)
    end do

    close(666)

end subroutine getparticleID_loopoverparticles



!######################################
!######################################
!######################################



subroutine mladen_particlecheck()
! this subroutine outputs all the interesting particle attributes.
! this subroutine is to check if the particle linked list really does
! what it's supposed to do.
    use amr_commons
    use pm_commons
    use clfind_commons
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif

    !filename
    character(len=80) :: fileloc
    character(len=5) :: nchar, nchar2

    !local vars
    integer :: i, part_ind, global_peak_id, local_peak_id, counter
    integer, dimension(:,:), allocatable:: totalparts_per_peak
    type (partll), pointer :: thispart_ll


    if(myid==1) write(*,*) "---Entered particlecheck"
    
    
    !generate filename
    call title(ifout-1, nchar)
    call title(myid, nchar2)


    allocate(totalparts_per_peak(1:npeaks_max, 1:2))
    ! 1: control. 2: linked list.
    totalparts_per_peak=0

    !################
    !CONTROL FILE
    !################
    fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_particlecontrol.txt'//nchar2)

    !#############################
    ! MAIN LOOP

    open(unit=666, file=fileloc, form='formatted')
    write(666, '(4A18)') "x", "y", "z", "clmp id"
    do i=1, npartmax
        if (levelp(i)>0) then
            write(666, '(3E18.9E2,I18)') xp(i,1), xp(i,2), xp(i,3), clumpidp(i)
            call get_local_peak_id(clumpidp(i), local_peak_id)
            totalparts_per_peak(local_peak_id,1)=totalparts_per_peak(local_peak_id,1)+1
        end if
    end do

    close(666)


    !################
    ! LINKED LIST FILES
    !################

    fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_particlelinkedlist.txt'//nchar2)

    !#############################
    ! MAIN LOOP

    !counter=0
    open(unit=666, file=fileloc, form='formatted')
    write(666, '(4A18)') "x", "y", "z", "clmp id"
    do i=1, npeaks_max
        thispart_ll=>clumpparticles(i,1)
        do while (associated(thispart_ll%next))
            part_ind=thispart_ll%particle_index 
            !if (part_ind>0) then
                write(666, '(3E18.9E2,I18)') xp(part_ind,1), xp(part_ind,2), xp(part_ind,3), clumpidp(part_ind)
                totalparts_per_peak(i,2)=totalparts_per_peak(i,2)+1
            !else
                !counter=counter+1
            !end if
            thispart_ll=>thispart_ll%next
        end do
    end do


    close(666)


    !write(*,*) myid, counter


    !#########################
    !Total particle comparison
    !#########################

    fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_totalpart.txt'//nchar2)
    open(unit=666, file=fileloc, form='formatted')
    write(666, '(3A14)') "ID", "Control", "Linked list"
    do i=1, npeaks
        global_peak_id=i+ipeak_start(myid)
        write(666, '(3I14)') global_peak_id, totalparts_per_peak(i,1), totalparts_per_peak(i,2) 
    end do
    close(666)

end subroutine mladen_particlecheck


!######################################
!######################################
!######################################
subroutine mladen_getmasscomparison2()

    ! Get mass comparison using the clump linked lists
    use amr_commons
    use clfind_commons   !unbinding stuff is all in here   
    use pm_commons !using mp
    use amr_parameters
    use hydro_commons !using mass_sph
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif

    integer :: global_peak_id, local_peak_id
    
    !getting particle mass
    real(dp) :: particle_mass, particle_mass_tot

    ! peak array
    real(dp), dimension(:), allocatable :: particle_sum_mass, particle_sum_mass_tot, clump_mass_array, clump_mass_array_tot

    !output related
    type (partll), pointer :: thispart_ll
    integer :: part_ind, i, info

    !filename
    character(len=80) :: fileloc
    character(len=5) :: nchar

    !#############################
    !#############################

    if(myid==1) write(*,*) "---Entered getmasscomparison"

    !get particle mass (copied from subroutine write_clump_properties)
    if(ivar_clump==0)then
        particle_mass=MINVAL(mp, MASK=(mp.GT.0.))
#ifndef WITHOUTMPI  
        call MPI_ALLREDUCE(particle_mass,particle_mass_tot,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,info)
    particle_mass=particle_mass_tot  
#endif
    else
        if(hydro)then
            particle_mass=mass_sph
        endif
    endif


    !allocate peak and clump_mass arrays
   
    allocate(particle_sum_mass(1:npeaks_tot), particle_sum_mass_tot(1:npeaks_tot), clump_mass_array(1:npeaks_tot), clump_mass_array_tot(1:npeaks_tot))

    particle_sum_mass = 0.0d0
    particle_sum_mass_tot = 0.0d0
    clump_mass_array_tot = 0.0d0
    clump_mass_array = 0.0d0


    !if(myid==1) write(*,*) "---Entered getmasscomparison1"
    !#############################
    ! particle mass sum
   
    do i=1, npeaks_max
        thispart_ll=>clumpparticles(i,1)
        do while (associated(thispart_ll%next))
            part_ind=thispart_ll%particle_index
            particle_sum_mass(clumpidp(part_ind))= particle_sum_mass(clumpidp(part_ind))+mp(part_ind)
            thispart_ll=>thispart_ll%next
        end do
    end do

    !if(myid==1) write(*,*) "---Entered getmasscomparison2"
    call MPI_ALLREDUCE(particle_sum_mass, particle_sum_mass_tot, npeaks_tot, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, info)
    
    !clump mass array
    do local_peak_id = 1, npeaks
        global_peak_id = local_peak_id + ipeak_start(myid)
        if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
            clump_mass_array(global_peak_id) = clump_mass(local_peak_id)
        end if
    end do
    call MPI_ALLREDUCE(clump_mass_array, clump_mass_array_tot, npeaks_tot, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, info)

    
    
    !if(myid==1) write(*,*) "---Entered getmasscomparison3"
    if (myid==1) then 
        !generate filename
        call title(ifout-1, nchar)
        fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_masscomparison.txt')

        open(unit=666, file=fileloc, form='formatted')

        write(666, '(4A20)') "peak index", "mass particles sum", "mass clump", "m_psum/mp_cl"
        !global_peak_id = 0
        do global_peak_id = 1, npeaks_tot
            if (clump_mass_array_tot(global_peak_id)/=0) then
                !write only relevant stuff. Relevant stuff has clump mass array /=0.
                write(666, '(I20, 3E20.9E2)') global_peak_id, &
                particle_sum_mass_tot(global_peak_id), &
                clump_mass_array_tot(global_peak_id),&
                particle_sum_mass_tot(global_peak_id)/clump_mass_array_tot(global_peak_id)
            else
                write(666, '(I20, 3E20.9E2)') global_peak_id, 0.0, 0.0, 0.0
            end if
        end do

        close(666)
    endif
    !if(myid==1) write(*,*) "---Entered getmasscomparison4"
end subroutine mladen_getmasscomparison2



!######################################
!######################################
!######################################



subroutine getparticleID_loopoverparticles_use_nvector()
    ! This subroutine gets the clump ID for every particle by
    ! looping over all particles instead of test cells.
    ! The reason for this subroutine is to check if I really
    ! get all the particles in a cell.

   ! Get mass comparison using the clump linked lists
    use amr_commons
    use clfind_commons   !unbinding stuff is all in here   
    use pm_commons !using mp
    use amr_parameters
    use hydro_commons !using mass_sph
    implicit none
    
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif

    integer, dimension(1:nvector) ::  cell_index, cell_levl
    integer, dimension(1:npeaks_max) :: particlesum_control
    integer :: global_peak_id, local_peak_id
    real(dp), dimension(1:nvector, 1:3) :: xcoord

    real(dp) :: particle_mass, particle_mass_tot
    integer :: i, j,ilevel, info, ivector

    !filename
    character(len=80) :: fileloc
    character(len=5) :: nchar, nchar2


    particlesum_control=0

    if(myid==1) write(*,*) "---Entered getparticleID_loopoverparticles"

    !get particle mass (copied from subroutine write_clump_properties)
    if(ivar_clump==0)then
        particle_mass=MINVAL(mp, MASK=(mp.GT.0.))
#ifndef WITHOUTMPI  
        call MPI_ALLREDUCE(particle_mass,particle_mass_tot,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,info)
    particle_mass=particle_mass_tot  
#endif
    else
        if(hydro)then
            particle_mass=mass_sph
        endif
    endif



    ivector=0
    do i= 1, npartmax
        if (levelp(i)>0) then
            cell_index=0
            cell_levl=0
            ivector=ivector+1
            xcoord(ivector,1:3)=xp(i,1:3)
            if (ivector==nvector) then
                call get_cell_index(cell_index, cell_levl,xcoord,nlevelmax,nvector)
                do j=1,nvector
                    global_peak_id=flag2(cell_index(j))
                    if (global_peak_id>0) then
                        call get_local_peak_id(global_peak_id, local_peak_id)
                        if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
                            clumpid_control(i-nvector+j) = global_peak_id
                            !particlesum_control(local_peak_id)=particlesum_control(local_peak_id)+1
                        end if
                    end if
                end do
                ivector=0
            end if
        end if
    end do

    if(ivector>0) then
        call get_cell_index(cell_index, cell_levl,xcoord,nlevelmax,ivector)
        do j=1,nvector
            global_peak_id=flag2(cell_index(j))
            if (global_peak_id>0) then
                call get_local_peak_id(global_peak_id, local_peak_id)
                if(relevance(local_peak_id) > relevance_threshold .and. halo_mass(local_peak_id) > mass_threshold*particle_mass) then
                    clumpid_control(npartmax-ivector+j) = global_peak_id
                    !particlesum_control(local_peak_id)=particlesum_control(local_peak_id)+1
                end if
            end if
        end do
    end if


!    call title(ifout-1, nchar)
    !call title(myid, nchar2)
    !
    !fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_getparticleID_loopoverparticles.txt'//TRIM(nchar2))
    !
    !open(unit=666, file=fileloc, form='formatted')
    !
    !write(666,'(2A12)') "Peak ID", "partcls in clump"
    !
    !do i=1,npeaks
    !    write(666,'(2I12)') i+ipeak_start(myid), particlesum_control(i)
    !end do
    !
    !close(666)

end subroutine getparticleID_loopoverparticles_use_nvector()



!######################################
!######################################
!######################################



real(dp) function enclosed_mass(ipeak, distance)
    !This function interpolates the enclosed mass of a sphere
    !with center in the CoM of a clump and radius equal to
    !the particle's distance to the CoM.
    !Optimisation idea: write separate functions for log and lin bins.
    !calculate binwidth (binwidths if log) and periodicity_correction-clump_com_pb 
    !only once per peak and give it to the function as an argument.
    use pm_commons
    use clfind_commons
    implicit none

    integer, intent(in) :: ipeak
    
    real(dp),intent(in) :: distance !is computed in function 'unbound', then passed
    integer :: ibin

    if (logbins) then
        !write(*,*) "No logbins defined yet."
        ibin=1
        do
            if (distance<=cmp_distances(ipeak,ibin)) then
                enclosed_mass=(cmp(ipeak,ibin)-cmp(ipeak,ibin-1))/(log(cmp_distances(ipeak,nmassbins))-log(rmin))*nmassbins*(log(distance)-log(cmp_distances(ipeak,ibin)))+cmp(ipeak,ibin)
                exit
            else
                ibin=ibin+1
            end if
        end do
    else
        !linear bins
        ibin=1
        do 
            if (distance<=cmp_distances(ipeak,ibin)) then
                enclosed_mass=(cmp(ipeak,ibin)-cmp(ipeak,ibin-1))/(cmp_distances(ipeak,ibin)-cmp_distances(ipeak,ibin-1))*(distance-cmp_distances(ipeak,ibin-1))+cmp(ipeak,ibin-1)
                exit
            else
                ibin=ibin+1
            end if
        end do
    end if !logbins or not
    
end function enclosed_mass



!######################################
!######################################
!######################################



logical function unbound(ipeak, part_ind)

    ! This function checks if the particle of clump ipeak
    ! with particle index part_ind is bound to the clump or not.
    ! It returns TRUE if the particle is not energetically bound.

    use pm_commons
    use clfind_commons
    implicit none

    integer, intent(in) :: ipeak, part_ind
    real(dp) :: distance, kinetic_energy, abs_phi

    distance=(xp(part_ind,1)+periodicity_correction(ipeak,1)-clump_com_pb(ipeak,1))**2 + &
            (xp(part_ind,2)+periodicity_correction(ipeak,2)-clump_com_pb(ipeak,2))**2 + &
            (xp(part_ind,3)+periodicity_correction(ipeak,3)-clump_com_pb(ipeak,3))**2
    distance=sqrt(distance)


    kinetic_energy=0.5*((vp(part_ind,1)-clump_vel_pb(ipeak,1))**2 + &
            (vp(part_ind,2)-clump_vel_pb(ipeak,2))**2 + &
            (vp(part_ind,3)-clump_vel_pb(ipeak,3))**2)
    
    !potential=factG*enclosed_mass(ipeak,distance)/distance
    abs_phi=potential(ipeak,distance)


    unbound=(kinetic_energy>=abs_phi)


end function unbound


!------------------------------------------------------------


real(dp) function potential(ipeak, distance)

    !This function interpolates the enclosed mass of a sphere
    !with center in the CoM of a clump and radius equal to
    !the particle's distance to the CoM.
    !It returns the absolute value of the potential.
    use pm_commons
    use clfind_commons
    implicit none

    integer, intent(in) :: ipeak
    real(dp),intent(in) :: distance !is computed in function 'unbound', then passed

    integer :: ibin, thisbin
    real(dp) :: a, b, enclosed_mass, mass_int
    !linear interpolation: expecting M(r) = a*r+b between bins.
    !log interpol: expecting M(r)=a*log(r)+b between bins.

   
    ibin=1
    thisbin=1
    do 
        if (distance<=cmp_distances(ipeak,ibin)) then
            thisbin=ibin
            exit
        else
            ibin=ibin+1
        end if
    end do


    if (logbins) then
        a = a_log(ipeak,thisbin)
        b = b_log(ipeak,thisbin,a)
    else
        a = a_lin(ipeak,thisbin)
        b = b_lin(ipeak,thisbin,a)
    end if

    if (mp_pot) then ! if point-mass potential
        if (logbins) then 
            enclosed_mass=a*log(distance)+b
        else
            enclosed_mass=a*distance+b
        end if

        potential=abs(factG*enclosed_mass/distance)

    else
        if (logbins) then 
            mass_int=encl_mass_int_log(ipeak,ibin,a,b,distance)
        else
            mass_int=encl_mass_int_lin(ipeak,thisbin,a,b,distance)
        end if

        potential=abs(factG*(mass_int+cmp(ipeak,nmassbins)/cmp_distances(ipeak,nmassbins)))
    end if



end function potential




!------------------------------------------------------------



real(dp) function a_lin(ipeak,ibin)
    use clfind_commons
    implicit none
    integer, intent(in) :: ipeak,ibin

    a_lin=(cmp(ipeak,ibin)-cmp(ipeak,ibin-1))/(cmp_distances(ipeak,ibin)-cmp_distances(ipeak,ibin-1))

end function a_lin


!------------------------------------------------------------


real(dp) function b_lin(ipeak,ibin, a)
    use clfind_commons
    implicit none
    integer, intent(in) :: ipeak,ibin
    real(dp), intent(in) :: a

    b_lin=cmp(ipeak,ibin-1)-a*cmp_distances(ipeak,ibin-1)

end function b_lin


!------------------------------------------------------------


real(dp) function a_log(ipeak,ibin)
    use clfind_commons
    implicit none
    integer, intent(in) :: ipeak,ibin

    a_log=(cmp(ipeak,ibin)-cmp(ipeak,ibin-1))/(log(cmp_distances(ipeak,ibin)/cmp_distances(ipeak,ibin-1)))
    !delta ln(r) =( ln(r_max) - ln(r_min) ) / nbins = ln (r_{i+1}) - ln(r_i)

end function a_log


!------------------------------------------------------------


real(dp) function b_log(ipeak,ibin,a)
    use clfind_commons
    implicit none
    integer, intent(in) :: ipeak,ibin
    real(dp), intent(in) :: a

    b_log= cmp(ipeak,ibin) - log(cmp_distances(ipeak,ibin))*a

end function b_log


!------------------------------------------------------------


real(dp) function encl_mass_int_lin(ipeak,ibin,a,b,distance)
    !compute the enclosed mass integral
    use clfind_commons
    implicit none
    integer, intent(in) :: ipeak, ibin
    real(dp), intent(in) ::a,b,distance

    integer :: i
    real(dp) :: calc, a_new

    
    calc=a*log(cmp_distances(ipeak,ibin)/distance) - b*(1./cmp_distances(ipeak,ibin)-1./distance)

    do i = ibin, nmassbins-1
        a_new=a_lin(ipeak,i+1)
        calc = calc + a_new*log(cmp_distances(ipeak,i+1)/cmp_distances(ipeak,i)) - b_lin(ipeak,i+1,a_new) *(1./cmp_distances(ipeak,i+1)-1./cmp_distances(ipeak,i))
    end do
    encl_mass_int_lin = calc 


end function encl_mass_int_lin
!########################################
!########################################
!########################################
subroutine get_periodicity_correction()

    use amr_commons
    use pm_commons
    use clfind_commons
    implicit none
    
    integer :: ipeak, i, ind
    ! periodicity
    logical, dimension(1:3) :: period

    period(1)=(nx==1)
    period(2)=(ny==1)
    period(3)=(nz==1)


    !GET PERIODICITY CORRECTION for virtual peaks
    !do this for virtual peaks only.
    do ipeak=1,npeaks_max
        if(nclmppart(ipeak)>0) then !if there is a particle
            ind=clmppart_first(ipeak)
            do i=1,3
                if (period(i) .and. xp(ind,i)-peak_pos(ipeak,i)>0.5*boxlen) periodicity_correction(ipeak,i)=(-1.0)*boxlen
                if (period(i) .and. xp(ind,i)-peak_pos(ipeak,i)<(-0.5)*boxlen) periodicity_correction(ipeak,i)=boxlen
            end do
        end if
    end do
end subroutine get_periodicity_correction
!########################################
!########################################
!########################################
subroutine mladen_particlecount(before)

    use clfind_commons
    use amr_commons
    use mladen_temp
    implicit none
    integer :: ipeak
    integer :: thispart, i, new_lid
    logical :: before
    character(len=80) :: fileloc
    character(len=5)  :: nchar, nchar2
    integer, dimension(1:npeaks_max) :: comparenr

    npart_ll=0
    do ipeak=1,hfree-1
!write(*,*) "Started counting for real peak ", ipeak+ipeak_start, " on id ", myid
        i=0
        !if (lev_peak(ipeak)==ilevel) then
            if(nclmppart(ipeak)>0) then
                thispart=clmppart_first(ipeak)
                do 
                    if (thispart==0) then
call get_local_peak_id(new_peak(ipeak),new_lid)
write(*,*) "!!!!!!!!!!!!!!!!!!!!!Found 0 for real peak ", ipeak+ipeak_start(myid), "iter", i,nclmppart(ipeak), "//", new_peak(ipeak)
                        exit
                    else
                        i=i+1
                        npart_ll(ipeak)=npart_ll(ipeak)+1
                        if (thispart==clmppart_last(ipeak)) then
                            exit
                        else
                            thispart=clmppart_next(thispart)
                        end if
                    end if
                end do
            end if
        !end if
    end do


    
    call title(ifout-1, nchar)
    call title(myid, nchar2)
    if(before) then
        fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_llparticles_before.txt'//nchar2)
    else
        fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_llparticles_after.txt'//nchar2)
    end if

    comparenr=nclmppart
    call build_peak_communicator()
    call virtual_peak_int(npart_ll(1),'sum')
    call boundary_peak_int(npart_ll)
    call virtual_peak_int(comparenr(1),'sum')
    call boundary_peak_int(comparenr)
    open(unit=666,file=fileloc,form='formatted')
    write(666,'(3A18)') "peak id", "particles in LL", "nclumppart"
    do ipeak=1, hfree-1
        if (npart_ll(ipeak)>0) then
            !if(npart_ll(ipeak).ne.nclmppart(ipeak)) then
                !write(*,*) "LEVEL", ilevel, "BEFORE", before, "PEAK", ipeak+ipeak_start(myid), "PEAK", ipeak+ipeak_start(myid)
                !write(*,*) "PEAK", ipeak+ipeak_start(myid), "PEAK", ipeak+ipeak_start(myid), npart_ll(ipeak)-nclmppart(ipeak)
                write(666,'(3I18)') ipeak+ipeak_start(myid), npart_ll(ipeak),comparenr(ipeak)
            !end if
        end if
    end do
    close(666)

end subroutine mladen_particlecount
!########################################
!########################################
!########################################

