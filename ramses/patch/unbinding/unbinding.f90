!! Patch written by Mladen Ivkovic (mladen.ivkovic@uzh.ch)
!!
!! New namelist parameters for this pach:
!! (Can be set in the CLUMPFIND_PARAMS block)
!!
!! NAME                        DEFAULT VALUE        FUNCTION
!! unbind=                     .true.               Turn particle unbinding on 
!!                                                  or off
!!
!! nmassbins=                  100                  Number of bins for the mass 
!!                                                  binning of the cumulative
!!                                                  mass profile. Any integer >1.
!!
!! logbins=                    .true.               use logarithmic binning 
!!                                                  distances for cumulative mass
!!                                                  profiles (and gravitational 
!!                                                  potential of clumps).
!!                                                  If false, the code  will use 
!!                                                  linear binning distances.
!!
!! unbinding_formatted_output= .false.              Create formatted output for 
!!                                                  particles, cumulative mass
!!                                                  profiles, binning distances, 
!!                                                  particle based clump
!!                                                  properties, gravitational 
!!                                                  potential of substructure
!!                                                  clumps 
!!
!! iter_properties=            .false.              whether to unbind multiple times 
!!                                                  with updated clump properties
!!                                                  determined by earlier unbindings
!! conv_limit =                0.1                  convergence limit. If the 
!!                                                  v_clump_old/v_clump_new < conv_limit,
!!                                                  stop iterating for this clump. 
!!
!! repeat_max =                100                  maximal number of loops per level
!!                                                  for iterative unbinding
!!                                                  (in case a clump doesn't converge)


!! New subroutines for this patch are:
!! subroutine unbinding()
!! subroutine get_clumpparticles()
!! subroutine get_clump_properties_pb()
!! subroutine get_cmp
!! subroutine get_closest_border
!! subroutine unbinding_neighborsearch
!! subroutine bordercheck
!! subroutine particle_unbinding()
!!      contains function unbound
!!      contains function potential
!! subroutine compute_phi
!! subroutine allocate_unbinding_arrays()
!! subroutine deallocate_unbinding_arrays()
!! subroutine unbinding_write_formatted_output()
!! subroutine unbinding_formatted_particleoutput()

#if NDIM==3

subroutine unbinding()

    use amr_commons !MPI stuff
    use pm_commons
    use clfind_commons !unbinding stuff

    implicit none
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif

    !------------------------------------------------------------
    ! This subroutine assigns all particles that are in cells
    ! identified to be in a clump by the clump finder the peak
    ! ID of that clump and checks whether they are energetically
    ! bound to that structure. If not, they are passed on to the
    ! clump's parents.
    !------------------------------------------------------------

    integer                     :: ipeak, info, ilevel, ipart, i
    integer                     :: loop_counter=0
    integer, dimension(1:npart) :: clump_ids
    character(LEN=80)           :: fileloc
    character(LEN=5)            :: nchar,nchar2
    logical                     :: loop_again_global, is_final_round, is_first



    !Logging/Announcing stuff
    if(myid==1) write(*,*) "Started unbinding."


    !------------------------
    ! Initial set-up
    !------------------------

    !update boundary relevance
    call build_peak_communicator
    call boundary_peak_dp(relevance)
    !call boundary_peak_int(new_peak)   !already done in clump_finder, doesn't 
                                        !need an update
    call boundary_peak_int(lev_peak)

  
    ! set up constants/
    GravConst=1d0     ! Gravitational constant
    if(cosmo)GravConst=3d0/8d0/3.1415926*omega_m*aexp

    periodic=(nx==1) !true if periodic
    nunbound=0       !count unbound particles
    candidates=0     !count unbinding candidates: Particles of child clumps
                     !that will be tested. If a particle is passed on to the
                     !parent clump, it will be counted twice.

    ! allocate necessary arrays
    call allocate_unbinding_arrays()



    !-------------------
    ! Gather particles 
    !-------------------

    !Get particles in substructrue, create linked lists
    call get_clumpparticles()

    !write output if required. This gives the particle clump assignment as given
    !by PHEW.
    if (unbinding_formatted_output) call unbinding_formatted_particleoutput(.true.) 



    !------------------
    ! Unbinding loop
    !------------------

    ! go level by level
    do ilevel=0, mergelevel_max
        !Start iteration loop 

        is_final_round=.true.
        if (iter_properties) is_final_round=.false.
        
        loop_again=.true.
        loop_counter=0

        !reset values
        to_iter = .true.
        hasatleastoneptcl=1


        do while(loop_again)
            loop_again = .false.  !set boolean whether to continue to false as default;
                                  !will get reset if conditions are met

            loop_counter=loop_counter+1
            niterunbound=0

            !get particle based clump properties :
            !Center of Mass, bulk velocity, particle furthest away from CoM
            is_first = (loop_counter==1)
            call get_clump_properties_pb(ilevel,is_first)

            if (loop_counter==repeat_max) loop_again=.false.

#ifndef WITHOUTMPI
            !sync with other processors wheter you need to repeat loop
            call MPI_ALLREDUCE(loop_again, loop_again_global, 1, MPI_LOGICAL, MPI_LOR,MPI_COMM_WORLD, info)

            loop_again=loop_again_global
#endif

            ! get cumulative mass profiles
            call get_cmp(ilevel)

            ! get closest border to the center of mass
            if (saddle_pot) call get_closest_border(ilevel)

            !UNBINDING LOOP
            if (.not.loop_again) is_final_round=.true.
            do ipeak=1, hfree-1
                if (cmp_distances(ipeak,nmassbins)>0.0 .and. lev_peak(ipeak)==ilevel) then 
                    ! use last cmp_distances as criterion because 
                    ! that value is communicated across processing units
                    call particle_unbinding(ipeak,is_final_round)
                end if
            end do

            if (loop_again) then
                !communicate whether peaks have remaining contributing particles
                call build_peak_communicator()
                call boundary_peak_int(hasatleastoneptcl,'max')
                call virtual_peak_int(hasatleastoneptcl)
                do ipeak=1,hfree-1
                    !if peak has no contributing particles anymore
                    if (hasatleastoneptcl(ipeak)==0) to_iter(ipeak)=.false. !dont loop anymore over this peak
                end do
            end if


            if (clinfo .and. iter_properties) then 
                !---------------------
                ! Talk to me.
                !---------------------

#ifndef WITHOUTMPI
                call MPI_ALLREDUCE(niterunbound, niterunbound_tot, 1, MPI_INTEGER, MPI_SUM,MPI_COMM_WORLD, info)
#else
                niterunbound_tot=niterunbound
#endif
                if (myid==1) then
                    write(*,'(A10,I10,A30,I5,A7,I5)') " Unbound", niterunbound_tot, "particles at level", ilevel, "loop", loop_counter
                end if
            end if


            if (.not. loop_again .and. clinfo .and. myid==1 .and. iter_properties .and. loop_counter < repeat_max) then
                write(*, '(A7,I5,A35,I5,A12)') "Level ", ilevel, "clump properties converged after ", loop_counter, "iterations."
            end if

            if (loop_counter==repeat_max) write(*,'(A7,I5,A20,I5,A35)') "Level ", ilevel, "not converged after ", repeat_max, "iterations. Moving on to next step."

        end do ! loop again for ilevel

    end do !loop over levels
    






    if (clinfo) then
#ifndef WITHOUTMPI
        call MPI_ALLREDUCE(nunbound, nunbound_tot, 1, MPI_INTEGER, MPI_SUM,MPI_COMM_WORLD, info)
        call MPI_ALLREDUCE(candidates, candidates_tot, 1, MPI_INTEGER, MPI_SUM,MPI_COMM_WORLD, info)
#else
        nunbound_tot=nunbound
        candidates_tot=candidates
#endif
        if (myid==1) then
            write(*,'(A6,I10,A30,I10,A12)') " Found", nunbound_tot, "unbound particles out of ", candidates_tot, " candidates"
        end if
    end if
     


    !-----------------
    ! Write output
    !-----------------

    if(unbinding_formatted_output) call unbinding_write_formatted_output()
    
    call title(ifout-1, nchar)
    call title(myid, nchar2)
    fileloc=TRIM('output_'//TRIM(nchar)//'/unbinding.out'//TRIM(nchar2))

    open(unit=666,file=fileloc,form='unformatted')
    
    ipart=0
    do i=1,npartmax
        if(levelp(i)>0)then
            ipart=ipart+1
            clump_ids(ipart)=clmpidp(i)
        end if
    end do
    write(666) clump_ids
    close(666)


    !--------------------
    ! Deallocate arrays
    !--------------------
    call deallocate_unbinding_arrays()

    !--------------------
    ! Say good bye.
    !--------------------
    if(verbose.or.myid==1) write(*,*) "Finished unbinding."

end subroutine unbinding
!######################################
!######################################
!######################################
subroutine get_clumpparticles()

    !---------------------------------------------------------------------------
    ! This subroutine loops over all test cells and assigns all particles in a
    ! testcell the peak ID the testcell has. If the peak is not a namegiver
    ! (= not its own parent), the particles are added to a linked list for 
    ! unbinding later.
    !---------------------------------------------------------------------------

    use amr_commons
    use clfind_commons      !unbinding stuff is all in here
    use pm_commons !using mp
    use amr_parameters
    use hydro_commons !using mass_sph
    implicit none
    
#ifndef WITHOUTMPI
    integer :: info
    include 'mpif.h'
#endif
    ! for looping over test cells and getting particle list
    integer   :: itestcell, ipart,this_part, global_peak_id, local_peak_id, prtcls_in_grid 
    
    ! getting particles per peak
    integer   :: ind, grid

    !getting particle mass
    real(dp)  :: particle_mass, particle_mass_tot

    !getting in which cell of a grid a particle is
    integer   :: part_cell_ind,i,j,k

    !appending linked lists
    integer   :: ipeak, new_peak_local_id, ilevel


    if(verbose) write(*,*) "Entered get_clumpparticles"

    !get particle mass (copied from subroutine write_clump_properties)
    if(ivar_clump==0)then
        particle_mass=MINVAL(mp, MASK=(mp.GT.0.))
#ifndef WITHOUTMPI  
        call MPI_ALLREDUCE(particle_mass,particle_mass_tot,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,info)
        particle_mass=particle_mass_tot  
#endif
    else
        if(hydro)then
            particle_mass=mass_sph
        endif
    endif

    !if(myid==1) write(*,*) "--- Particle mass:", particle_mass


    !-----------------------------------------------------------
    ! Get particles from testcells into linked lists for clumps
    !-----------------------------------------------------------

    do itestcell=1, ntest !loop over all test cells
        global_peak_id=flag2(icellp(itestcell))

        if (global_peak_id /= 0) then

            ! get local peak id
            call get_local_peak_id(global_peak_id, local_peak_id)

            ! If peak ID is also a halo, there is no need for particle unbinding,
            ! so there is no need for a linked list of particles.
            ! This check is also important for the relevance conditions:
            ! There are clumps that satisfy the halo mass condition, but do not
            ! satisfy the clump relevance condition. If not separated, the 
            ! halos who are not relevant clumps will not be recognised.

            ! Check if peak is halo:            
            if (new_peak(local_peak_id)==global_peak_id) then
            
                !------------------
                !Found a halo cell
                !------------------

                ! if peak relevant:
                if(halo_mass(local_peak_id) > mass_threshold*particle_mass) then
                
                    ind=(icellp(itestcell)-ncoarse-1)/ngridmax+1    ! get cell position
                    grid=icellp(itestcell)-ncoarse-(ind-1)*ngridmax ! get grid index
                    prtcls_in_grid = numbp(grid)                    ! get number of particles in grid
                    this_part=headp(grid)                           ! get index of first particle
                
                    ! If it is a halo: only assign particles the ID
                    ! loop over particles in grid
                    do ipart = 1, prtcls_in_grid
            
                        !check cell index of particle so you loop only once over each
                        i=0
                        j=0
                        k=0
                        if(xg(grid,1)-xp(this_part,1)/boxlen+(nx-1)/2.0 .le. 0) i=1
                        if(xg(grid,2)-xp(this_part,2)/boxlen+(ny-1)/2.0 .le. 0) j=1
                        if(xg(grid,3)-xp(this_part,3)/boxlen+(nz-1)/2.0 .le. 0) k=1
                
                        part_cell_ind=i+2*j+4*k+1
                        
                        !If index is correct, assign clump id to particle
                        if (part_cell_ind==ind) clmpidp(this_part)=global_peak_id
                        !go to next particle in this grid
                         this_part = nextp(this_part)
                    end do
                end if

            else != if not halo ID

                !---------------------
                !Found a subhalo cell
                !---------------------

                if (relevance(local_peak_id) > relevance_threshold) then
                    ! If clump isn't also halo: assign ID to particles,
                    ! create linked particle list
                    
                    ind=(icellp(itestcell)-ncoarse-1)/ngridmax+1    ! get cell position
                    grid=icellp(itestcell)-ncoarse-(ind-1)*ngridmax ! get grid index
                    prtcls_in_grid = numbp(grid)                    ! get number of particles in grid
                    this_part=headp(grid)                           ! get index of first particle

                   
                    !loop over particles in grid
                        do ipart=1, prtcls_in_grid
                            !check cell index of particle so you loop only once over each
                            i=0
                            j=0
                            k=0
                            if(xg(grid,1)-xp(this_part,1)/boxlen+(nx-1)/2.0 .le. 0) i=1
                            if(xg(grid,2)-xp(this_part,2)/boxlen+(ny-1)/2.0 .le. 0) j=1
                            if(xg(grid,3)-xp(this_part,3)/boxlen+(nz-1)/2.0 .le. 0) k=1
    
                            part_cell_ind=i+2*j+4*k+1
            
                            ! If index is correct, assign clump id to particle
                            if (part_cell_ind==ind) then
                                !assign peak ID
                                clmpidp(this_part)=global_peak_id
                                !add particle to linked list of clumpparticles 
                                !check if already particles are assigned
                                if (nclmppart(local_peak_id)>0) then
                                    !append to the last particle of the list
                                    clmppart_next(clmppart_last(local_peak_id))=this_part
                                else
                                    !assign particle as first particle
                                    !for this peak of linked list 
                                    clmppart_first(local_peak_id)=this_part
                                end if
                                !update last particle for this clump
                                nclmppart(local_peak_id)=nclmppart(local_peak_id)+1
                                clmppart_last(local_peak_id)=this_part
                            end if    
                            !go to next particle in this grid
                            this_part=nextp(this_part)
                        end do
                end if !if clump is relevant
            end if     !if clump or halo
        end if         !global peak /=0
    end do             !loop over test cells




    !------------------------------------------------------
    !Append substructure particles to parents' linked list
    !------------------------------------------------------
    
    ! must be done level by level!
    do ilevel=0,mergelevel_max
        do ipeak=1, hfree-1
            ! append substructure linked lists to parent linked lists
            if(lev_peak(ipeak)==ilevel) then
                if (nclmppart(ipeak)>0) then
                    ! get local id of parent
                    call get_local_peak_id(new_peak(ipeak),new_peak_local_id)
                    ! append particle LL to parent's LL if parent isn't a halo-namegiver
                    if(new_peak(ipeak)/=new_peak(new_peak_local_id)) then !if peak isnt namegiver
                        ! It might happen that the parent peak doesn't have a 
                        ! particle linked list yet (on this processor).
                        if (nclmppart(new_peak_local_id)>0) then !particle ll exists
                            clmppart_next(clmppart_last(new_peak_local_id))=clmppart_first(ipeak)
                        else
                            clmppart_first(new_peak_local_id)=clmppart_first(ipeak)
                        end if

                        clmppart_last(new_peak_local_id)=clmppart_last(ipeak)
                        nclmppart(new_peak_local_id)=nclmppart(new_peak_local_id)+nclmppart(ipeak)
                    end if
                end if
            end if
        end do
    end do

end subroutine get_clumpparticles
!########################################
!########################################
!########################################
subroutine get_clump_properties_pb(ilevel,first)
    use amr_commons
    use pm_commons
    use clfind_commons
    implicit none
#ifndef WITHOUTMPI
    include 'mpif.h'
#endif
    logical, intent(in) :: first  !if it is the first time calculating
    integer, intent(in) :: ilevel !clump level currently on
   
    !--------------------------------------------------------------------------
    ! This subroutine computes the particle-based properties of the clumps:
    ! namely the center of mass and the clump's velocity.
    ! If it's called for the first time, it will compute the properties for
    ! all peak IDs. If not, it will go level by level.
    !--------------------------------------------------------------------------

    !particle furthest away
    real(dp) :: distance, biggest

    ! iterators
    integer :: ipeak, i, ipart
    integer :: thispart
    real(dp) :: vsq
    real(dp),dimension(1:3) :: period
    real(dp),dimension(1:npeaks_max) :: clmp_vel_pb_old
    logical :: check

    if (verbose) write(*,*)"Entered get_clump_properties (particle based)"


    !------------------------------------------------------------
    ! If iterative: Store old values, reset virtual peak values
    !------------------------------------------------------------

    do ipeak=1, hfree-1
        check = .not. first
        check = check .and. lev_peak(ipeak) == ilevel
        check = check .and. to_iter(ipeak)
        if (check) then
            clmp_vel_pb_old(ipeak)=clmp_vel_pb(ipeak,1)**2+clmp_vel_pb(ipeak,2)**2+clmp_vel_pb(ipeak,3)**2
            do i=1,3
                oldcom(ipeak,i)=clmp_com_pb(ipeak,i)
                oldvel(ipeak,i)=clmp_vel_pb(ipeak,i)
            end do
            oldcmpd(ipeak)=cmp_distances(ipeak,nmassbins)
            oldm(ipeak)=clmp_mass_pb(ipeak)
        end if

        if (iter_properties .and. ipeak>npeaks) then  
            !for communication: set virtual peak values=0
            !so they won't contribute in the communication sum
            !reset values
            do i=1,3
                clmp_vel_pb(ipeak,i)=0.0
                clmp_com_pb(ipeak,i)=0.0
            end do
            clmp_mass_pb(ipeak)=0.0
        end if
    end do  


    !------------------------------------------------------
    ! GET CENTER OF MASS, CENTER OF MOMENTUM FRAME VELOCITY
    !------------------------------------------------------

    do ipeak=1, hfree-1 !loop over all peaks
        if (to_iter(ipeak).and.lev_peak(ipeak)==ilevel) then !if peak has particles and needs to be iterated over

            !reset values
            do i=1,3
                clmp_vel_pb(ipeak,i)=0.0
                clmp_com_pb(ipeak,i)=0.0
            end do
            cmp_distances(ipeak,nmassbins)=0.0
            clmp_mass_pb(ipeak)=0.0


            if (hasatleastoneptcl(ipeak)>0 .and. nclmppart(ipeak)>0) then 
                !if there is work to do on this processing unit for this peak

                thispart=clmppart_first(ipeak)
                
                do ipart=1, nclmppart(ipeak)           !while there is a particle linked list
                    if (contributes(thispart)) then    !if the particle should be considered
                        if (periodic) then !determine periodic correction
                            period=0.d0
                            do i=1, 3
                                if (xp(thispart,i)-peak_pos(ipeak,i) > 0.5*boxlen)    period(i)=(-1.0)*boxlen
                                if (xp(thispart,i)-peak_pos(ipeak,i) < (-0.5*boxlen)) period(i)=boxlen
                            end do
                        end if

                        clmp_mass_pb(ipeak)=clmp_mass_pb(ipeak)+mp(thispart)
                        do i=1,3
                            clmp_com_pb(ipeak,i)=clmp_com_pb(ipeak,i)+(xp(thispart,i)+period(i))*mp(thispart) !get center of mass sum               
                            clmp_vel_pb(ipeak,i)=clmp_vel_pb(ipeak,i)+vp(thispart,i)*mp(thispart) !get velocity sum
                        end do
                    else
                        contributes(thispart)=.true. !reset value
                    end if
                    thispart=clmppart_next(thispart) !go to next particle in linked list
                end do ! loop over particles
            end if     ! there is work for this peak on this processor
        end if         ! peak needs to be looked at
    end do             ! loop over peaks


    !----------------------------------------------------------------------
    ! communicate center of mass, clump mass and velocity across processors
    !----------------------------------------------------------------------
    call build_peak_communicator
    do i=1,3
        call virtual_peak_dp(clmp_com_pb(1,i),'sum')  !collect
        call boundary_peak_dp(clmp_com_pb(1,i))       !scatter
        call virtual_peak_dp(clmp_vel_pb(1,i),'sum')  !collect
        call boundary_peak_dp(clmp_vel_pb(1,i))       !scatter
    end do
    call virtual_peak_dp(clmp_mass_pb,'sum')          !collect
    call boundary_peak_dp(clmp_mass_pb)               !scatter


    do ipeak=1, hfree-1

        check = to_iter(ipeak)
        check = check .and. lev_peak(ipeak)==ilevel
        check = check .and. clmp_mass_pb(ipeak)>0

        if (check) then
            !calculate actual CoM and center of momentum frame velocity
            do i=1,3
                clmp_com_pb(ipeak,i)=clmp_com_pb(ipeak,i)/clmp_mass_pb(ipeak)
                clmp_vel_pb(ipeak,i)=clmp_vel_pb(ipeak,i)/clmp_mass_pb(ipeak)
            end do

            !------------------------------------
            !FIND PARTICLE FURTHEST AWAY FROM CoM
            !------------------------------------
            ! The maximal distance of a particle to the CoM is saved in the last
            ! cmp_distances array for every peak.
            if(nclmppart(ipeak)>0) then
                biggest=0.0
                thispart=clmppart_first(ipeak)
                do ipart=1, nclmppart(ipeak) !while there is a particle linked list

                    period=0.d0
                    
                    if (periodic) then
                        do i=1, 3
                            if (xp(thispart,i)-peak_pos(ipeak,i)>0.5*boxlen) period(i)=(-1.0)*boxlen
                            if (xp(thispart,i)-peak_pos(ipeak,i)<(-0.5*boxlen)) period(i)=boxlen
                        end do
                    end if

                    distance=(xp(thispart,1)+period(1)-clmp_com_pb(ipeak,1))**2 + &
                        (xp(thispart,2)+period(2)-clmp_com_pb(ipeak,2))**2 + &
                        (xp(thispart,3)+period(3)-clmp_com_pb(ipeak,3))**2

                    if(distance>biggest) biggest=distance ! save if it is biggest so far

                    thispart=clmppart_next(thispart)

                end do
                if (biggest>0.0) cmp_distances(ipeak,nmassbins)=sqrt(biggest) !write if you have a result
            end if !to iterate
        end if
    end do ! over all peaks

    !-------------------------------------------------
    !communicate distance of particle furthest away
    !-------------------------------------------------
    call build_peak_communicator
    call virtual_peak_dp(cmp_distances(1,nmassbins), 'max')
    call boundary_peak_dp(cmp_distances(1,nmassbins))



    !-------------------------------------------------
    ! If iterative clump properties determination:
    ! Check whether bulk velocity converged
    !-------------------------------------------------

    if (iter_properties) then !if clump properties will be determined iteratively
        do ipeak=1, hfree-1

            check = to_iter(ipeak)
            check = check .and. lev_peak(ipeak)==ilevel
            check = check .and. cmp_distances(ipeak,nmassbins)>0.0

            if (check) then
                vsq=clmp_vel_pb(ipeak,1)**2+clmp_vel_pb(ipeak,2)**2+clmp_vel_pb(ipeak,3)**2

                if ( abs( sqrt(clmp_vel_pb_old(ipeak)/vsq) - 1.0) < conv_limit ) then
                    to_iter(ipeak) = .false. ! consider bulk velocity as converged
                    !write(*,'(A8,I3,A15,I8,A6,E15.6E2,A5,E15.6E2,A7,E15.6E2,A9,E15.6E2)') &
                    !& "#####ID", myid, "clump CONVERGED", ipeak+ipeak_start(myid), "old:", &
                    !& clmp_vel_pb_old(ipeak), "new:", vsq, "ratio",  abs( sqrt(clmp_vel_pb_old(ipeak)/vsq) - 1.0),&
                    !& "v_bulk=", sqrt(vsq)
                else
                    loop_again=.true. !repeat
                end if
            end if
        end do
    end if


end subroutine get_clump_properties_pb 
!###################################
!###################################
!###################################
subroutine get_cmp(ilevel)

    use amr_commons
    use pm_commons
    use clfind_commons
    implicit none
    integer, intent(in) :: ilevel 

    !----------------------------
    !Get cumulative mass profiles
    !----------------------------

    integer  :: ipeak, i, ipart, levelmax
    real(dp) :: r_null, distance
    integer  :: thispart
    real(dp),dimension(1:3) :: period
    logical  :: check
   
#ifndef WITHOUTMPI
    integer  :: levelmax_glob, info
    include 'mpif.h'
#endif

    if(verbose) write(*,*) "Entered get cumulative mass profiles"

    if (logbins) then
        !get minimal distance:
        levelmax=0
        do i=1,nlevelmax
           if(numbtot(1,i)>0) levelmax=levelmax+1
        end do

#ifndef WITHOUTMPI
        ! get system-wide levelmax
        call MPI_ALLREDUCE(levelmax,levelmax_glob, 1, MPI_INTEGER, MPI_MAX,MPI_COMM_WORLD, info)

        levelmax=levelmax_glob
#endif

        rmin=boxlen/2**levelmax
    end if




    do ipeak=1, hfree-1

        !peak must have need to be reiterated
        check=to_iter(ipeak)
        check=check.and.lev_peak(ipeak)==ilevel            !peak must have correct level
        check=check.and.nclmppart(ipeak)>0                 !peak must have particles on this processor        

        !reset values
        if (check .or. ipeak > npeaks) then
            do i = 1, nmassbins
                cmp(ipeak,i) = 0.0
            end do
        end if


        if (check) then
            !------------------------------------------
            !Compute cumulative mass binning distances
            !------------------------------------------
            !The distances are not communicated later, but computed on each
            !processor independently, because each processor has all information it needs 
            !with cmp_distances(ipeak,nmassbins) and CoM

            if (logbins) then
                do i=1, nmassbins-1
                    cmp_distances(ipeak,i)=rmin*(cmp_distances(ipeak,nmassbins)/rmin)**(real(i)/real(nmassbins))
                end do
            else !linear binnings
                r_null=cmp_distances(ipeak,nmassbins)/real(nmassbins)
                do i=0, nmassbins-1
                    cmp_distances(ipeak,i)=r_null*i
                end do
            end if
            !The last bin must end with precicely with the maximal 
            !Distance of the particle. That is
            !needed because precision errors. The maximal distance is 
            !computed via particle data and the outermost bin is equal
            !to the distance of the outermost particle to the CoM.
            !Precision errors cause the code to crash here.

            !---------------------------------------------
            ! bin particles in cumulative mass profiles:
            ! get mass of each bin
            ! calculate particle distance to CoM
            !---------------------------------------------
            thispart=clmppart_first(ipeak)
            do ipart=1, nclmppart(ipeak)!while there is a particle linked list
                period=0.d0
                if (periodic) then
                    do i=1, 3
                        if (xp(thispart,i)-peak_pos(ipeak,i)>0.5*boxlen)    period(i)=(-1.0)*boxlen
                        if (xp(thispart,i)-peak_pos(ipeak,i)<(-0.5*boxlen)) period(i)=boxlen
                    end do
                end if
                distance=(xp(thispart,1)+period(1)-clmp_com_pb(ipeak,1))**2 + &
                    (xp(thispart,2)+period(2)-clmp_com_pb(ipeak,2))**2 + &
                    (xp(thispart,3)+period(3)-clmp_com_pb(ipeak,3))**2
                distance=sqrt(distance)


                i=1
                do 
                    if (distance<=cmp_distances(ipeak,i)) then
                        cmp(ipeak,i) = cmp(ipeak,i) + mp(thispart)
                        exit
                    else
                        i=i+1
                    end if
                end do

                thispart=clmppart_next(thispart)
            end do

            ! sum up masses to get profile instead of mass in shell
            do i=0,nmassbins-1
                cmp(ipeak,i+1)=cmp(ipeak,i+1)+cmp(ipeak,i) 
            end do

        end if  ! check
    end do      ! loop over peaks

    !--------------------------------------  
    !communicate cummulative mass profiles
    !--------------------------------------  
    call build_peak_communicator()
    do i=1,nmassbins
        call virtual_peak_dp(cmp(1,i), 'sum')
        call boundary_peak_dp(cmp(1,i)) 
    end do

end subroutine get_cmp
!########################################
!########################################
!########################################
subroutine get_closest_border(ilevel)
    use amr_commons
    use clfind_commons
    implicit none
    integer, intent(in) :: ilevel
    !---------------------------------------------------------------------------
    ! Find closest border to centre of mass. Modified subroutine saddlepoint_search
    !---------------------------------------------------------------------------
    integer                         ::  ipart,ipeak,ip,jlevel,next_level
    integer                         ::  local_peak_id,global_peak_id
    integer,dimension(1:nvector)    ::  ind_cell
    logical,dimension(1:npeaks_max) ::  check
  
    character(len=80) :: fileloc
    character(len=5)  :: nchar, nchar2

    if(verbose)write(*,*) "Entered get_closest_border"

    check=.false.
    do ipeak=1, hfree-1

        check(ipeak)=cmp_distances(ipeak,nmassbins)>0.0 !peak must have particles somewhere
        check(ipeak)=check(ipeak).and.to_iter(ipeak)
        check(ipeak)=check(ipeak).and.lev_peak(ipeak)==ilevel !peak must have correct level

        if(check(ipeak))  closest_border(ipeak) = 3.d0*boxlen**2 !reset value
    end do



    !-------------------------
    ! Loop over all testcells
    !-------------------------
    ip=0
    do ipart=1,ntest
        jlevel=levp(ipart) ! level
        next_level=0 !level of next particle
        if(ipart<ntest)next_level=levp(ipart+1)

        
        global_peak_id=flag2(icellp(ipart))
        if (global_peak_id/=0) then 

            call get_local_peak_id(global_peak_id,local_peak_id)

            if(check(local_peak_id)) then !if testcell is of interest:
                ip=ip+1
                ind_cell(ip)=icellp(ipart)
                if(ip==nvector .or. next_level /= jlevel)then
                    call unbinding_neighborsearch(ind_cell,ip,jlevel)
                    ip=0
                endif
            endif
        end if
    end do
    if (ip>0)call unbinding_neighborsearch(ind_cell,ip,jlevel)

    !------------------------
    ! Communicate results
    !------------------------

    call build_peak_communicator()
    call virtual_peak_dp(closest_border,'min')
    call boundary_peak_dp(closest_border)



    !------------------------
    ! Write output
    !------------------------

    if (unbinding_formatted_output) then
        call title(ifout-1, nchar)
        call title(myid, nchar2)

        fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_closestborders.txt'//nchar2)
        open(unit=666,file=fileloc,form='formatted')
        if (ilevel==mergelevel_max) write(666,'(4A18)') "peak id", "x", "y", "z"
        do ipart=1,npeaks
            if(cmp_distances(ipart,nmassbins)>0.and.ilevel==mergelevel_max) then
                write(666,'(I18,3E18.9E2)') ipart+ipeak_start(myid), closest_border(ipart)
            end if
        end do
        close(666)
    end if

end subroutine get_closest_border
!#####################################################
!#####################################################
!#####################################################
!#####################################################
subroutine unbinding_neighborsearch(ind_cell,np,jlevel)
  use amr_commons
  implicit none
  integer,dimension(1:nvector),intent(in) ::ind_cell    !array of indices of cells that I want to check
  integer,intent(in)                      ::np          !number of actual cells in ind_cell
  integer,intent(in)                      ::jlevel      !cell level

  !------------------------------------------------------------
  ! Modified subroutine neighborsearch
  ! This routine constructs all neighboring leaf cells at levels 
  ! jlevel-1, jlevel, jlevel+1.
  ! Then performs the check if the neighbors are a border
  ! in order to find the closest border to the center of mass
  !------------------------------------------------------------

  integer::j,ind,nx_loc,i1,j1,k1,i2,j2,k2,i3,j3,k3,ix,iy,iz
  integer::i1min,i1max,j1min,j1max,k1min,k1max
  integer::i2min,i2max,j2min,j2max,k2min,k2max
  integer::i3min,i3max,j3min,j3max,k3min,k3max
  real(dp)::dx,dx_loc,scale
  integer ,dimension(1:nvector)::clump_nr,indv,ind_grid,grid,ind_cell_coarse

  real(dp),dimension(1:twotondim,1:3)::xc
  integer ,dimension(1:99)::neigh_cell_index,cell_levl,test_levl
  real(dp),dimension(1:99,1:ndim)::xtest,xrel
  logical ,dimension(1:99)::ok
  real(dp),dimension(1:3)::skip_loc
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim),save::nbors_father_grids 
  integer::ntestpos,ntp,idim,ipos

  real(dp),dimension(1:3)::this_cellpos



    ! Mesh spacing in that level
    dx=0.5D0**jlevel 
    nx_loc=(icoarse_max-icoarse_min+1)
    !skip_loc=(/0.0d0,0.0d0,0.0d0/)
    skip_loc(1)=dble(icoarse_min)
    skip_loc(2)=dble(jcoarse_min)
    skip_loc(3)=dble(kcoarse_min)
    scale=boxlen/dble(nx_loc)
    dx_loc=dx*scale

    ! Integer constants
    i1min=0; i1max=1; i2min=0; i2max=2; i3min=0; i3max=3
    j1min=0; j1max=1; j2min=0; j2max=2; j3min=0; j3max=3
    k1min=0; k1max=1; k2min=0; k2max=2; k3min=0; k3max=3

    ! Cells center position relative to grid center position
    do ind=1,twotondim
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
    end do
  
    ! some preliminary action...
    do j=1,np
        indv(j)     = (ind_cell(j)-ncoarse-1)/ngridmax+1       ! cell position in grid
        ind_grid(j) = ind_cell(j)-ncoarse-(indv(j)-1)*ngridmax ! grid index
        clump_nr(j) = flag2(ind_cell(j))                       ! save clump number
    end do 




    ntestpos=3**ndim
    if(jlevel>levelmin)  ntestpos=ntestpos+2**ndim
    if(jlevel<nlevelmax) ntestpos=ntestpos+4**ndim

    !================================
    ! generate neighbors level jlevel-1
    !================================
    ntp=0
    if(jlevel>levelmin)then
        ! Generate 2x2x2  neighboring cells at level jlevel-1     
        do k1=k1min,k1max
            do j1=j1min,j1max
                do i1=i1min,i1max         
                    ntp=ntp+1
                    xrel(ntp,1)=(2*i1-1)*dx_loc
                    xrel(ntp,2)=(2*j1-1)*dx_loc
                    xrel(ntp,3)=(2*k1-1)*dx_loc
                    test_levl(ntp)=jlevel-1
                end do
            end do
        end do
    endif

    !================================
    ! generate neighbors at level jlevel
    !================================
    ! Generate 3x3x3 neighboring cells at level jlevel
    do k2=k2min,k2max
        do j2=j2min,j2max
            do i2=i2min,i2max
                ntp=ntp+1
                xrel(ntp,1)=(i2-1)*dx_loc
                xrel(ntp,2)=(j2-1)*dx_loc
                xrel(ntp,3)=(k2-1)*dx_loc
                test_levl(ntp)=jlevel
            end do
        end do
    end do
  
    !===================================
    ! generate neighbors at level jlevel+1
    !====================================
    if(jlevel<nlevelmax)then
        ! Generate 4x4x4 neighboring cells at level jlevel+1
        do k3=k3min,k3max
            do j3=j3min,j3max
                do i3=i3min,i3max
                    ntp=ntp+1
                    xrel(ntp,1)=(i3-1.5)*dx_loc/2.0
                    xrel(ntp,2)=(j3-1.5)*dx_loc/2.0
                    xrel(ntp,3)=(k3-1.5)*dx_loc/2.0
                    test_levl(ntp)=jlevel+1
                end do
            end do
        end do
    endif



    ! Gather 27 neighboring father cells (should be present anytime !)
    do j=1,np
        ind_cell_coarse(j)=father(ind_grid(j))
    end do
    call get3cubefather(ind_cell_coarse,nbors_father_cells,nbors_father_grids,np,jlevel)


    do j=1,np
        ok=.false.
        do idim=1,ndim
            !get real coordinates of neighbours
            xtest(1:ntestpos,idim)=(xg(ind_grid(j),idim)+xc(indv(j),idim)-skip_loc(idim))*scale+xrel(1:ntestpos,idim)
            if(jlevel>levelmin)xtest(1:twotondim,idim)=xtest(1:twotondim,idim)+xc(indv(j),idim)*scale
        end do
        grid(1)=ind_grid(j)
        call get_cell_index_fast(neigh_cell_index,cell_levl,xtest,ind_grid(j),nbors_father_cells(j,1:threetondim),ntestpos,jlevel)
     
        do ipos=1,ntestpos
            !make sure neighbour is a leaf cell
            if(son(neigh_cell_index(ipos))==0.and.cell_levl(ipos)==test_levl(ipos)) then
                ok(ipos)=.true. 
            end if
        end do
     
        !get position of the cell whose neighbours will be tested
        do idim=1,ndim
            this_cellpos(idim)=(xg(ind_grid(j),idim)+xc(indv(j),idim)-skip_loc(idim))*scale
        end do

        !check neighbors
!if (myid==1) write(*,*) "ID 1 calling bordercheck for peak", clump_nr(j)
        call bordercheck(this_cellpos,clump_nr(j),xtest,neigh_cell_index,ok,ntestpos)
        ! bordercheck (this_cellpos=position of cell to test;
        ! clump_nr(j)=peak ID of cell to test;
        ! xtest=positions of neighbour cells; 
        ! neigh_cell_index=index of neighbour cells;
        ! ok = if neighbour cell is leaf cell;
        ! ntestpos = how many neighbour cells there are
    end do

end subroutine unbinding_neighborsearch
!########################################
!########################################
!########################################
subroutine bordercheck(this_cellpos,clump_nr,xx,neigh_cell_index,ok,np)
    !----------------------------------------------------------------------
    ! routine to check wether neighbor belongs to another clump and is closer to the center
    ! of mass than all others before
    ! modified subroutine saddlecheck
    !----------------------------------------------------------------------
    use amr_commons
    use clfind_commons
    implicit none
    real(dp), dimension(1:np,1:ndim), intent(in)    :: xx         ! positions of neighbour cells
    real(dp), dimension(1:ndim),      intent(in)    :: this_cellpos   ! position of test cell whose neighbours 
                                                                  ! are to be tested
    integer,  dimension(1:99),        intent(in)    :: neigh_cell_index ! cell index of neighbours
    integer,                          intent(in)    :: clump_nr   ! global peak ID of cell whose neighbours
                                                                  ! will be tested
    logical,  dimension(1:99),        intent(inout) :: ok         ! wether cell should be checkedre
    integer,                          intent(in)    :: np         ! number of neighbours to be looped over


    real(dp), dimension(1:99,1:ndim)  :: pos        ! position of border for each neighbour
    integer,  dimension(1:99)         :: neigh_cl !clump number of neighbour,local peak id of neighbour
    real(dp)                          :: newsum
    integer                           :: i,j,ipeak
    real(dp), dimension(1:3)          :: period


    do j=1,np
        neigh_cl(j)=flag2(neigh_cell_index(j))!index of the clump the neighboring cell is in 

        ok(j)=ok(j).and. clump_nr/=0 ! temporary fix...
        ok(j)=ok(j).and. neigh_cl(j)/=0 !neighboring cell is in a clump. If neighbour not in clump, clump is still considered isolated.
        ok(j)=ok(j).and. neigh_cl(j)/=clump_nr !neighboring cell is in another clump 
    end do


    call get_local_peak_id(clump_nr,ipeak)

    do j=1,np
        if(ok(j))then ! if all criteria met, you've found a neighbour cell that belongs to a different clump 

            period=0.d0
            if (periodic) then
                do i=1, ndim
                    if (xx(j,i)-clmp_com_pb(ipeak,i) > 0.5*boxlen)    period(i)=(-1.0)*boxlen
                    if (xx(j,i)-clmp_com_pb(ipeak,i) < (-0.5*boxlen)) period(i)=boxlen
                end do
            end if

            do i=1, ndim
                !the cells will be nighbours, so no need to compute two different periodic corrections
                pos(j,i)=(xx(j,i)+period(i)+this_cellpos(i)+period(i))*0.5 
            end do

            newsum=0
            do i=1, ndim
                newsum=newsum+(pos(j,i)-clmp_com_pb(ipeak,i))**2
            end do

            if (newsum<closest_border(ipeak))  closest_border(ipeak)=newsum
            end if
    end do


end subroutine bordercheck
!########################################
!########################################
!########################################
subroutine particle_unbinding(ipeak,final_round)
    use amr_commons, only: dp
    use clfind_commons

    implicit none
    integer, intent(in) :: ipeak    !peak to loop over
    logical, intent(in) :: final_round !if it is the final round => whether to write
    !--------------------------------------------------------------
    !This subroutine loops over all particles in the linked list of
    !peak ipeak and checks if they are bound.
    !--------------------------------------------------------------

    integer :: thispart, ipeak_test, ipart
    real(dp):: phi_border !the potential at the border of the peak patch closest 
                          !to the center of mass
    real(dp):: dist_border  !distance to the border


    !compute the potential for this peak on the points of the mass bin distances
    call compute_phi(ipeak)

    !compute potential at the closest border from the center of mass
    phi_border=0.d0
    if(saddle_pot) then
        dist_border=sqrt(closest_border(ipeak))
        if(dist_border<=cmp_distances(ipeak,nmassbins)) phi_border=potential(ipeak,dist_border)
    end if


    thispart=clmppart_first(ipeak)


    if (final_round) then
        do ipart=1, nclmppart(ipeak)      ! loop over particle LL
            call get_local_peak_id(clmpidp(thispart),ipeak_test)
            if (ipeak_test==ipeak) then   ! if this particle needs to be checked for unbinding
                                          ! particle may be assigned to child clump
                candidates=candidates+1
                if(unbound(ipeak,thispart,phi_border)) then  
                    !unbound(ipeak,thispart,phi_border) is a logical function. See below.

                    nunbound=nunbound+1                      !counter
                    clmpidp(thispart)=new_peak(ipeak)        !update clump id
                end if
            end if
            thispart=clmppart_next(thispart)
        end do
    else 
        if (to_iter(ipeak)) then
            hasatleastoneptcl(ipeak)=0         ! set to false
            do ipart=1, nclmppart(ipeak)       ! loop over particle LL
                if(unbound(ipeak,thispart,phi_border)) then  
                    !unbound(ipeak,thispart,phi_border) is a logical function. See below.

                    niterunbound=niterunbound+1
                    contributes(thispart) = .false.   ! particle doesn't contribute to
                                                      ! clump properties
                else
                    hasatleastoneptcl(ipeak)=1 !there are contributing particles for this peak
                end if
                thispart=clmppart_next(thispart)
            end do

        end if 
    end if

!----------------------
!----------------------
!----------------------

contains

logical function unbound(ipeak, part_ind, phi_border)
    !-----------------------------------------------------------
    ! This function checks if the particle of clump ipeak
    ! with particle index part_ind is bound to the clump or not.
    ! It returns TRUE if the particle is not energetically bound.
    !-----------------------------------------------------------

    use pm_commons
    use clfind_commons
    implicit none

    integer, intent(in) :: ipeak, part_ind
    real(dp), intent(in) :: phi_border
    real(dp) :: distance, kinetic_energy, minusphi
    real(dp),dimension(1:3) :: period
    integer :: i

    period=0.d0
    if (periodic) then
        do i=1, ndim
            if (xp(part_ind,i)-clmp_com_pb(ipeak,i) > 0.5*boxlen   ) period(i)=(-1.0)*boxlen
            if (xp(part_ind,i)-clmp_com_pb(ipeak,i) < (-0.5*boxlen)) period(i)=boxlen
        end do
    end if

    distance=(xp(part_ind,1)+period(1)-clmp_com_pb(ipeak,1))**2 + &
            (xp(part_ind,2)+period(2)-clmp_com_pb(ipeak,2))**2 + &
            (xp(part_ind,3)+period(3)-clmp_com_pb(ipeak,3))**2
    distance=sqrt(distance)


    kinetic_energy=0.5*((vp(part_ind,1)-clmp_vel_pb(ipeak,1))**2 + &
            (vp(part_ind,2)-clmp_vel_pb(ipeak,2))**2 + &
            (vp(part_ind,3)-clmp_vel_pb(ipeak,3))**2)
    
    minusphi=potential(ipeak,distance)

    unbound=(kinetic_energy>=minusphi-phi_border)


end function unbound
!----------------------
!----------------------
!----------------------
real(dp) function potential(ipeak,distance)
    !------------------------------------------------------------------
    !This function interpolates the potential of a particle for given distance
    !It returns (-1)*phi
    !------------------------------------------------------------------

    integer, intent(in) :: ipeak
    real(dp),intent(in) :: distance !is computed in function 'unbound', then passed

    integer :: ibin, thisbin
    real(dp) :: a,b

    ibin=1
    !thisbin: the first cmp_distance which is greater than particle distance
    thisbin=1
    do 
        if (distance<=cmp_distances(ipeak,ibin)) then
            thisbin=ibin
            exit
        else
            ibin=ibin+1
        end if
    end do

    a=(phi_unb(thisbin)-phi_unb(thisbin-1))/(cmp_distances(ipeak,thisbin)-cmp_distances(ipeak,thisbin-1))
    b=phi_unb(thisbin-1)-a*cmp_distances(ipeak,thisbin-1)
    potential=(-1)*a*distance-b

end function potential 
end subroutine particle_unbinding
!###############################################
!###############################################
!###############################################
subroutine compute_phi(ipeak)
    !-----------------------------------------------------------
    ! This subroutine computes the potential on each massbin
    ! It writes potential[r=ibin] into the array phi_unb[ibin]
    !-----------------------------------------------------------
    use clfind_commons
    use amr_commons!, only: dp
    integer, intent(in) :: ipeak
    real(dp) :: delta,add
    integer  :: i

    !Writing unformatted output
    character(len=5)  :: bins
    character(len=5)  :: peak
    !character(len=10) :: peak !in case there are too many (>1E6) peaks
    character(len=80) :: fileloc
    character(len=5)  :: nchar

    !compute part of integral/sum for each bin
    phi_unb(nmassbins)=0.0
    do i=2,nmassbins
        delta=cmp_distances(ipeak,i)-cmp_distances(ipeak,i-1)
        phi_unb(i-1)=-0.5*GravConst*(cmp(ipeak,i)/cmp_distances(ipeak,i)**2+cmp(ipeak,i-1)/cmp_distances(ipeak,i-1)**2)*delta
    end do
    delta=cmp_distances(ipeak,1)-cmp_distances(ipeak,0)
    phi_unb(0)=-0.5*GravConst*(cmp(ipeak,1)/cmp_distances(ipeak,1)**2)*delta

    !sum bins up
    !does not need to be done for i=nmassbins!
    add=-cmp(ipeak,nmassbins)/cmp_distances(ipeak,nmassbins)*GravConst !-G*M_tot/r_max
    do i=nmassbins-1,0,-1
        phi_unb(i)=phi_unb(i)+phi_unb(i+1) !stopps at phi_unb(1)
        phi_unb(i+1)=phi_unb(i+1)+add
    end do
    phi_unb(0)=phi_unb(0)+add !bypass division by 0, needed for interpolation.


    if (unbinding_formatted_output) then
    !WARNING:
    !Crashes if there are more than 10^6 peaks because it creates a file with ****** in the name,
    !and multiple processes try to write into same file.
    !This happens because the subroutine title() only goes up to 5 digits.
    !Instead, switch to commented out part. (Also the other declaration for char peaks)
        if (ipeak<=npeaks) then
            !generate filename integers
            call title(ifout-1, nchar)
            call title(nmassbins,bins)
            call title(ipeak+ipeak_start(myid),peak)
            !write(peak,'(i10)') ipeak+ipeak_start ! in case of too many peaks

            fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_phi_'//TRIM(peak)//'-'//TRIM(bins)//'.txt')
            open(unit=667,file=fileloc,form='formatted')
            write(667,'(2A20)') "distance", "potential"
            do i=0,nmassbins
                write(667,'(2E20.8E2)') cmp_distances(ipeak,i), phi_unb(i)
            end do
            close(667)
            
            fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_cmp_'//TRIM(peak)//'-'//TRIM(bins)//'.txt')
            open(unit=666,file=fileloc,form='formatted')
            write(666,'(2A20)') "distance", "cumulative mass"
            do i=0,nmassbins
                write(666,'(2E20.8E2)') cmp_distances(ipeak,i), cmp(ipeak,i)
            end do
            close(666)

            fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_clumpproperties_'//TRIM(peak)//'.txt')
            open(unit=666, file=fileloc, form='formatted')
            write(666, '(A10,7A18)') "clmp id", "x", "y ", "z", "vx", "vy", "vz", "max_dist"
            if (clmp_mass_pb(ipeak)>0) then
                write(666,'(I10,7E18.9E2)') ipeak+ipeak_start(myid), clmp_com_pb(ipeak,1), clmp_com_pb(ipeak,2), clmp_com_pb(ipeak,3), clmp_vel_pb(ipeak,1), clmp_vel_pb(ipeak,2),clmp_vel_pb(ipeak,3), cmp_distances(ipeak, nmassbins)
            end if
            close(666)
        end if
    end if

end subroutine compute_phi 
!###############################################
!###############################################
!###############################################
subroutine allocate_unbinding_arrays()
    use clfind_commons
    use pm_commons, only:npartmax
    implicit none

    !----------------------------------------------
    ! This subroutine allocates the necessary 
    ! arrays and gives them initial values.
    !----------------------------------------------

    !-------------------
    ! Clump properties
    !-------------------
    allocate(clmp_com_pb(1:npeaks_max,1:3))
    clmp_com_pb=0.0
    allocate(clmp_vel_pb(1:npeaks_max,1:3))
    clmp_vel_pb=0.0
    allocate(clmp_mass_pb(1:npeaks_max))
    clmp_mass_pb=0.0
    allocate(cmp_distances(1:npeaks_max,0:nmassbins))
    cmp_distances=0.0
    allocate(cmp(1:npeaks_max,0:nmassbins))
    cmp=0.d0
    ! careful with this! The first index of the second subscript
    ! of the cumulative mass aray (index 0) is there for reference
    ! for the enclosed mass interpolation.

    allocate(phi_unb(0:nmassbins)) ! array where to store the potential
    phi_unb=0.d0

    if (saddle_pot) then
        allocate(closest_border(1:npeaks_max)) !point of the closest border to CoM
        closest_border=3.d0*boxlen**2
    end if

    allocate(to_iter(1:npeaks_max)) ! peak needs to be checked or not
    to_iter=.true.

    if (iter_properties) then
        allocate(oldcom(1:npeaks_max,1:3))
        allocate(oldvel(1:npeaks_max,1:3))
        allocate(oldcmpd(1:npeaks_max))
        allocate(oldm(1:npeaks_max))
    end if

    allocate(hasatleastoneptcl(1:npeaks_max))
    hasatleastoneptcl=1 !initiate to yes


    !----------------------
    ! Particle linked list
    !----------------------
    allocate(clmpidp(1:npartmax))
    clmpidp=0

    allocate(clmppart_first(1:npeaks_max)) !linked lists containing particles 
    clmppart_first=0

    allocate(clmppart_last(1:npeaks_max)) !linked lists containing particles 
    clmppart_last=0

    allocate(clmppart_next(1:npartmax)) !linked lists containing particles 
    clmppart_next=0

    allocate(nclmppart(1:npeaks_max)) !linked lists containing particles 
    nclmppart=0

    allocate(contributes(1:npartmax)) !particle contributes to clump properties or not
    contributes=.true.



end subroutine allocate_unbinding_arrays
!########################################
!########################################
!########################################
subroutine deallocate_unbinding_arrays()
    use clfind_commons
    implicit none

    deallocate(clmp_com_pb)
    deallocate(clmp_vel_pb)
    deallocate(clmp_mass_pb)
    deallocate(cmp_distances)
    deallocate(cmp)

    deallocate(phi_unb)

    if(saddle_pot) deallocate(closest_border)

    deallocate(to_iter)

    if (iter_properties) deallocate(oldcom,oldvel,oldcmpd,oldm)

    deallocate(hasatleastoneptcl)


    deallocate(clmpidp)
    deallocate(clmppart_last)
    deallocate(clmppart_first)
    deallocate(clmppart_next)
    deallocate(nclmppart)
    deallocate(contributes)

end subroutine deallocate_unbinding_arrays
!############################################
!############################################
!############################################
subroutine unbinding_write_formatted_output()

    !------------------------------------------------------------------
    ! This subroutine outputs all the interesting particle attributes.
    !------------------------------------------------------------------
    
    use amr_commons
    use pm_commons
    use clfind_commons
    implicit none
    !select which output(s)
    logical :: particles, CoM, mass_prof, dist


    !iterators
    integer:: ipeak, i

    !filename
    character(len=80) :: fileloc
    character(len=5) :: nchar, nchar2


    ! set which output you want here:
    particles=.true.    ! all particles: coordinates, velocity, clump ID
    CoM=.true.          ! for all clumps which are not halo-namegivers:
                        ! clump id, PHEW peak position, particlebased Center
                        ! of Mass, maximal distance of particles to CoM
    dist=.false.         ! mass bin distances
    mass_prof=.false.    ! mass profiles; binning width is bin distances


    !generate filename integers
    call title(ifout-1, nchar)
    call title(myid, nchar2)
    
    if(particles) call unbinding_formatted_particleoutput(.false.)


    if (CoM) then
        fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_COM.txt'//nchar2)
    
        open(unit=666, file=fileloc, form='formatted')
        write(666, '(A10,7A18)') "clmp id", "x", "y ", "z", "vx", "vy", "vz", "max_dist"
        do ipeak=1, npeaks
            if (clmp_mass_pb(ipeak)>0) then
                write(666,'(I10,7E18.9E2)') ipeak+ipeak_start(myid), clmp_com_pb(ipeak,1), clmp_com_pb(ipeak,2), clmp_com_pb(ipeak,3), clmp_vel_pb(ipeak,1), clmp_vel_pb(ipeak,2),clmp_vel_pb(ipeak,3), cmp_distances(ipeak, nmassbins)
            end if
        end do
        close(666)
    end if


    !CUMULATIVE MASS PROFILES
    if (mass_prof) then
        if (logbins) then
            fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_CMP-log.txt'//nchar2)
        else
            fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_CMP-lin.txt'//nchar2)
        end if
        
        open(unit=666, file=fileloc, form='formatted')
        write(666,'(A10)',advance='no') ipeak+ipeak_start(myid)
        do i=1, nmassbins
            write(666,'(I18)',advance='no') i
        end do
        write(666,*)
        do ipeak=1, npeaks
            if (cmp_distances(ipeak,nmassbins)>0.0) then
                write(666,'(I10)',advance='no') ipeak+ipeak_start(myid)
                do i=1, nmassbins
                    write(666,'(E18.9E2)',advance='no') cmp(ipeak,i)
                end do
                write(666,*)
            end if
        end do
        close(666)
    end if



    !MASS PROFILE BIN DISTANCES
    if (dist) then
        fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_distances.txt'//nchar2)

        open(unit=666, file=fileloc, form='formatted')
        write(666,'(A10)',advance='no') ipeak+ipeak_start(myid)
        do i=1, nmassbins
            write(666,'(I18)',advance='no') i
        end do
        write(666,*)
        do ipeak=1, npeaks
            if (cmp_distances(ipeak,nmassbins)>0.0) then
                write(666,'(I10)',advance='no') ipeak+ipeak_start(myid)
                do i=1, nmassbins
                    write(666,'(E18.9E2)',advance='no') cmp_distances(ipeak,i)
                end do
                write(666,*)
            end if
        end do
        close(666)
    end if


end subroutine unbinding_write_formatted_output
!###############################################
!###############################################
!###############################################
subroutine unbinding_formatted_particleoutput(before)

    !--------------------------------------------------------------------
    ! This subroutine writes all the interesting particle attributes to
    ! file. 
    ! If before = .true. (called before the unbinding starts), it will 
    ! create a new directory "before" in the output directory and
    ! write the particle attributes as found by PHEW to file.
    !--------------------------------------------------------------------

    use amr_commons
    use pm_commons
    use clfind_commons
    implicit none
#ifndef WITHOUTMPI
    include 'mpif.h'
    integer :: info
#endif

    logical,intent(in) :: before
    
    !filename
    character(len=80) :: fileloc
    character(len=5)  :: nchar, nchar2

    !local vars
    integer           :: i
    character(len=80) :: cmnd

    if (before) then

        if (myid==1) then ! create before dir
            call title(ifout-1,nchar)
            cmnd='mkdir -p output_'//TRIM(nchar)//'/before'
            call system(TRIM(cmnd))
        end if

#ifndef WITHOUTMPI
    call MPI_BARRIER(MPI_COMM_WORLD,info)
#endif

    end if


    !generate filename
    call title(ifout-1, nchar)
    call title(myid, nchar2)

    !todo: change mladen?
    if (before) then
        fileloc=TRIM('output_'//TRIM(nchar)//'/before/mladen_particleoutput.txt'//nchar2)
    else
        fileloc=TRIM('output_'//TRIM(nchar)//'/mladen_particleoutput.txt'//nchar2)
    end if

 

    open(unit=666, file=fileloc, form='formatted')
    write(666, '(10A18)') "x", "y", "z", "vx", "vy", "vz", "clmp id", "times unbound", "mass", "pid"
    do i=1, npartmax
        if(levelp(i)>0) then
            write(666, '(6E18.9E2,2I18,E18.9E2,I18)') xp(i,1), xp(i,2), xp(i,3), vp(i,1), vp(i,2), vp(i,3), clmpidp(i),-666,mp(i),idp(i)
        end if 
    end do

    close(666)
end subroutine unbinding_formatted_particleoutput
!#############################################
!#############################################
!#############################################

! endif: NDIM == 3
#endif
